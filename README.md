<!--- *************************************************************************
      * Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
      *
      * Permission is hereby granted, free of charge, to any person obtaining a copy
      * of this software and associated documentation files (the "Software"), to deal
      * in the Software without restriction, including without limitation the rights
      * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
      * copies of the Software, and to permit persons to whom the Software is
      * furnished to do so, subject to the following conditions:
      *
      * The above copyright notice and this permission notice shall be included in
      * all copies or substantial portions of the Software.
      *
      * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
      * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
      * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
      * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
      * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
      * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
      * SOFTWARE.
      *
      * SPDX-License-Identifier: MIT
      *
      * Contributors: 
      *      Nikolaos Tepelidis - Author
      *      Vasileios Siopidis - Couthor
      *      Konstantinos Votis - Couthor
      ************************************************************************* --->

<h2>Description </h2>

![architecture_Anom](docs/figures/arch_extended_anom.png)

The anonymization tool is a tool where the user can upload a csv file and through a process to extract the data he wants anonymized. More specifically the user after importing the dataset (csv file) into the Datamite framework will be informed about the sensitive columns of the csv and will be suggested anonymization techniques in each column depending on its content. Some examples of sensitive attributes are gender, ethnicity, religion, race and age, just to name a few. Some examples of anonymization techniques are generalization, suppression, masking, pseydoanonymization e.t.c.  Then once the user selects the desired technique for the desired csv column, the tool, after saving the anonymized dataset in the database, will provide it to the user in a downloadable format, while giving him the possibility to check if the privacy-preserving techniques (k-anonymity, l-diversity, t-closeness) are verified in his dataset.

The following schema illustrates a systematic process for implementing a supportive tool, as flow diagram.

![Alt text](/docs/figures/anonymization_architecture.png)

<h2>Architecture </h2>

The anonymization tool emcompasses an standalone service for DATAMITE user. The architecture of the service is divided in six components.
1.	Sensitive data recognition module, where the tool recognizes which data of the dataset are sensitive and inform the user
2.	Anonymization technique suggestion module, where the tool recognizes the nature of the dataset fields and suggest the appropriate anonymization technique
3.	Anonymization process module, where the tool anonymize the data of the imported dataset 
4.	Anonymization check module, where the tool check if the privacy-preserving techniques (k-anonymity, l-diversity, t-closeness) are verified in the anonymized dataset
5.	Anonymization tool back-end 
6.	Anonymization tool front-end

![Alt text](/docs/figures/image-1.png)

<h2>Installation and requirements </h2>

<h4>Programming Languages </h4>

For this tool we will use Python as primary language to implement the functionalities. It is not necessary to have it installed in order to test the project on your local environment. For that purpose, the project uses Docker for a containerized execution.
Libraries

For this tool we will use the libraries that mentioned below:

•	[NLTK](https://www.nltk.org/): Natural Language Toolkit.

•	[Pandas](https://pandas.pydata.org/): Python Data Analysis Library.


<h4>Running the Anonymization tool </h4>

The anonymization tool is a web-based tool based on python programming language. To run the anonymization firstly, you need to download the repo. The repo includes source code (src folder), Dockerfile and .yaml file, which are required to run the tool and a “requirements.txt” file, which includes the necessary libraries to run the code. You can build and run the code by navigating to the path of anonymization tool through cmd and type: 

`docker-compose up –-build`

Note 1: In order to run the tool you should create first a network and run the data product composition by following the instructions that given in this [repo](https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-sharing/data-product-composition) . Also in the link above you can find instructions of how to configure the database for data product composition which is common for both tools. 

Note 2: In order to run docker command you need to install docker on your operating system.
After code build successfully you can access swagger documentation of anonymization tool through any web browser by typing: 

http://127.0.0.1:5000/anonymization/


<h4>Databases </h4>

The database that used in the tool is PostgreSql. 


<h2>Roadmap</h2>

![Alt text](/docs/figures/image-2.png)

<h2>Indicative endpoints</h2>

•	Description: This endpoint handles dataset upload by user 

```
Input:
  HTTP Method: Post 

  Endpoint: ‘api/load_data’ 

  Headers: Content-Type: application/json 

  Body: {"file": "dataset.csv"} 

Response:
{“Status Code”: “200 Dataset uploaded successfully”}

```
•	Description: This endpoint handles the process of sensitive columns identification. 

```
Input:
HTTP Method: Post 

  Endpoint: ‘api/identify_columns’ 

  Headers: Content-Type: application/json 



Response:

{"identiied_columns": "....."}
```

•	Description: This endpoint gives more information about identified sensitive columns. 

```
Input:
HTTP Method: Get 

  Endpoint: ‘api/get_sensitive_columns’ 

  Headers: Content-Type: application/json 



Response:

{"get_sensitive_columns": "type_of_data", "number_of_records", "number_of_errors", "3_rows_of_every_column"}
```

•	Description: This endpoint gives the possibility to the user to add which sensitive columns he wants and take the anonymization techniques for those columns. 

```
Input:
HTTP Method: Post 

  Endpoint: ‘api/post_column_info’ 

  Headers: Content-Type: application/json 

  Body: ["column_name1", "column_name2"]

Response:

{"anonymization_techniques": "generalization", "masking", "suppression", "pseydo-anonymization"}
```

•	Description: This endpoint handles the process of anonymizing data and save the anonymized dataset to the database. 

```
Input:
HTTP Method: Post 

  Endpoint: ‘api/post_anonymization_info’ 

  Headers: Content-Type: application/json 

  Body: ["column_name1", "column_name2"], [“anonymization_technique1”, “anonymization_technique2”]

Response:

{“Status Code”: “200 Anonymization success and anonymized data saved into the database”}
```



•	Description: This endpoint handles the process of retrieving data from the user. 
```
Input:
HTTP Method: Get 

  Endpoint: ‘api/download_anonymized_file’ 

  Headers: Content-Type: application/json 

  Body: {"format": "xlsx/json/csv"}
```

