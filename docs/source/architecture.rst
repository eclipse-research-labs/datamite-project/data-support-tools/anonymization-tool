Architecture and Design
=====

.. _diagram:

Architecture Diagram
------------

The architecture Diagram, as displayed below, presents the high-lever overview of the logging tool service's functions
within the DATAMITE system, in particular with the Data sharing Module. Focusing on logging various actions and data.

.. image:: https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-sharing/logging-tool/-/raw/main/logging/images/abtract_architecture.png?ref_type=heads

