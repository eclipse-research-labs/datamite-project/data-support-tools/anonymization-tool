Developer Guide
====

 **An upper level description functionalities of the tool**


It is an indpepentent web service made on node.js, mysql running on an nginx web server.
Docker tools are also used so the components of the app are build and avialiable as docker images.
The authentication will be under a Datamite keycloak server.


  **Some key points of the tool are**
====================================================



*To log by saving on a database various actions of Datamite services like the requests made to get some dataset by a consumer.


*To log by saving on a database the creation, update, negociate action on an agreement(not only the action but store both the data agreement and its hash for validation).


*There will be a dashboard to visualise the logged data and provide various metrics and  filters, the logging service will provide the appropiate methods to get these data on the dashboard.


*Depending the volume of the data and the requested format of the representation that is required the logged data also should be saved with various aggregated formats for quick access.



