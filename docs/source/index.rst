
#############################
Datamite's Anonymisation tool
#############################


***************
Contents:
***************

.. toctree::
   
   introduction
   architecture
   installation
   developer_guide
   user_guide
   license
   contribution_and_community


.. image:: https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-support-tools/anonymization-tool/-/raw/main/docs/figures/arch_extended_anom.png?ref_type=heads

.. note::

   This project is under active development.
