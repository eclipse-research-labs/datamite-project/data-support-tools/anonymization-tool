# Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 
# SPDX-License-Identifier: MIT
# 
# Contributors:
#     Nikolaos Tepelidis - Author
#     Vasileios Siopidis - Couthor
#     Konstantinos Votis - Couthor

import pandas as pd
import numpy as np
from flask import  jsonify
from generalization_process import generalize_age, generalize_address, generalize_post_code, generalize_salary, generalize_payment, generalize_income, generalize_bill, generalisation_custom_field
from drop_column_process import drop_column
from pseydoanonymization_process import pseydoanonymization, pseydoanonymization_custom, pseydoanonymization_hashing, pseudonymisation_format_preserving_custom
from masking_process import masking, mask_custom_field 
sensitive_columns_energy = []
sensitive_columns_telecommunication = []
sensitive_columns_agriculture = []
sensitive_columns_meteo = []
general_sensitive_columns = ["name", "surname", "age", "post_code", "address", "town", "country", "phone_number", "email", "vat", "gender", "salary", "credit_card","debit_card", "social_security", "passport_number", "id", "username", "password", "ip_address", "mac_address", "coordinates", "bank_account", "iban", "payment", "income", "ethnicity", "religion", "customer", "supplier", "bill"]
general_sensitive_columns_gr = ["ονομα", "επιθετο", "ηλικια", "ΤΚ", "διευθυνση", "πολη", "χωρα", "κινητο_τηλεφωνο", "email", "ΑΦΜ", "φυλο", "μισθος", "αριθμος_χρεωστικης_καρτας","αριθμος_πιστωτικης_καρτας", "ΑΜΚΑ", "αριθμος_διαβατηριου", "αριθμος_ταυτοτητας", "χρηστης", "κωδικος_προσβασης", "συντεταγμενες", "αριθμος_λογαριασμου", "πληρωμη", "αποδοχες", "εθνικοτητα", "θρησκευμα", "πελατης", "προμηθευτης", "λογαριασμος"]
sensitive_columns = general_sensitive_columns + sensitive_columns_energy + sensitive_columns_telecommunication + sensitive_columns_agriculture + sensitive_columns_meteo
column_names_generalization = ["age", "address", "post_code", "salary", "payment", "income", "bill"]
column_names_generalization_gr = ["ηλικια", "διευθυνση", "ΤΚ", "μισθος", "πληρωμη", "αποδοχες", "λογαριασμος"]
column_names_masking = ["age", "post_code", "email", "vat", "phone_number", "salary", "credit_card","debit_card", "social_security", "ip_address", "mac_address", "bank_account", "iban", "id", "passport_number", "username"]
column_names_masking_gr = ["ηλικια", "ΤΚ", "κινητο_τηλεφωνο", "ΑΦΜ", "μισθος", "αριθμος_χρεωστικης_καρτας","αριθμος_πιστωτικης_καρτας", "ΑΜΚΑ", "αριθμος_διαβατηριου", "αριθμος_ταυτοτητας", "χρηστης", "αριθμος_λογαριασμου"]
column_names_drop_column = general_sensitive_columns
column_names_drop_column_gr = general_sensitive_columns_gr
column_names_pseydoanonymization_substitution = ["post_code", "address", "phone_number", "vat", "social_security", "passport_number", "id", "bank_account", "iban"]
column_names_pseydoanonymization_gr_substitution = ["ΤΚ", "διευθυνση", "κινητο_τηλεφωνο", "ΑΦΜ", "ΑΜΚΑ", "αριθμος_διαβατηριου", "αριθμος_ταυτοτητας", "αριθμος_λογαριασμου"]
column_names_pseydoanonymization_format_preserving = ["name", "surname", "age", "town", "country", "email", "gender", "salary", "credit_card","debit_card", "ip_address", "mac_address", "income", "ethnicity", "religion", "customer", "supplier"]
column_names_pseydoanonymization_gr_format_preserving = ["ονομα", "επιθετο", "ηλικια", "πολη", "χωρα", "φυλο", "μισθος", "αριθμος_χρεωστικης_καρτας","αριθμος_πιστωτικης_καρτας", "αποδοχες", "εθνικοτητα", "θρησκευμα", "πελατης", "προμηθευτης"]
column_names_pseydoanonymization_hashing = ["vat", "credit_card", "debit_card", "passport_number", "id", "username", "password", "ip_address", "mac_address", "coordinates"]
column_names_pseydoanonymization_hashing_gr = ["ΑΦΜ", "αριθμος_χρεωστικης_καρτας", "αριθμος_πιστωτικης_καρτας", "αριθμος_διαβατηριου", "αριθμος_ταυτοτητας", "χρηστη", "κωδικος_προσβασης", "συντεταγμενες"]
column_names_pseydoanonymization_custom = ["name", "surname", "age", "post_code", "address", "town", "country", "phone_number", "email", "vat", "gender", "salary", "credit_card","debit_card", "social_security", "passport_number", "id", "ip_address", "mac_address", "bank_account", "iban", "income", "ethnicity", "religion", "customer", "supplier"]
column_names_pseydoanonymization_gr_custom = ["ονομα", "επιθετο", "ηλικια", "ΤΚ", "διευθυνση", "πολη", "χωρα", "κινητο_τηλεφωνο", "email", "ΑΦΜ", "φυλο", "μισθος", "αριθμος_χρεωστικης_καρτας","αριθμος_πιστωτικης_καρτας", "ΑΜΚΑ", "αριθμος_διαβατηριου", "αριθμος_ταυτοτητας", "αριθμος_λογαριασμου",  "αποδοχες", "εθνικοτητα", "θρησκευμα", "πελατης", "προμηθευτης"]

user_selected_custom_fields = [] 
def update_user_selected_custom_fields(df, selected_fields, selected_column1):
    """Updates the global list of user-selected custom fields and decides on generalisation availability."""
    global user_selected_custom_fields
    global generalisation_custom_enabled  # New global flag to track generalisation availability

    # Debug prints
    print("DataFrame Columns:", df.columns.tolist())  # Print DataFrame columns as a list
    print("Selected Fields:", selected_fields)  # Print selected fields

    # Update the user-selected custom fields
    user_selected_custom_fields = list(set(selected_fields))  # Remove duplicates

    # Check if any selected field is numeric (integer or float)
    numeric_fields = [col for col in selected_column1 if col in df.columns and df[col].dtype in ['int64', 'float64']]

    # Set the flag for generalisation_custom based on numeric fields availability
    generalisation_custom_enabled = bool(numeric_fields)
    print(f"Generalisation Custom Enabled: {generalisation_custom_enabled}")  # Debug print

    
    
def post_column_info(selected_column, source="custom"):
    try:
        print(f"Processing column: {selected_column}, Source: {source}")
        allowed_techniques = get_allowed_anonymization_techniques(selected_column, source)
        print(f"Allowed Techniques for {selected_column}: {allowed_techniques}")

        if not isinstance(allowed_techniques, list):  # Ensure it's a list
            raise ValueError(f"Invalid return type from get_allowed_anonymization_techniques: {allowed_techniques}")
        
        return {'status': 'success', 'allowed_techniques': allowed_techniques}
    except Exception as e:
        return {'status': 'error', 'message': f'Error in post_column_info: {str(e)}'}

def post_anonymization_info(df, selected_column, selected_technique, selected1_column):
    try:
        # Apply anonymization to the selected column based on the chosen technique
        print(f"Anonymized value for '{selected_column}': {selected_technique}")
        anonymized_df = anonymize_column(df, selected_column, selected_technique, selected1_column)
        if anonymized_df is None:
            raise ValueError('The anonymized DataFrame is None.')

        # Create a JSON representation of the anonymized DataFrame
        anonymized_value = anonymized_df.to_json(orient='records')

        # Return success status
        return {'status': 'success', 'anonymized_value': anonymized_value}
    except Exception as e:
        return {'status': 'error', 'message': f'Error in post_anonymization_info: {str(e)}'}


def get_allowed_anonymization_techniques(selected_column, source="general"):
    """
    Get allowed anonymization techniques based on whether the column is a general or custom field.
    """
    # Define the techniques for general sources
    techniques_mapping_general = {
        'generalisation': column_names_generalization + column_names_generalization_gr,
        'masking': column_names_masking + column_names_masking_gr,
        'suppression': column_names_drop_column + column_names_drop_column_gr,
        'pseudonymisation_substitution': column_names_pseydoanonymization_substitution + column_names_pseydoanonymization_gr_substitution,
        'pseudonymisation_hashing': column_names_pseydoanonymization_hashing + column_names_pseydoanonymization_hashing_gr,
        'pseudonymisation_format_preserving': column_names_pseydoanonymization_format_preserving + column_names_pseydoanonymization_gr_format_preserving
    }
    
    # Define the techniques for custom sources
    techniques_mapping_custom = {
        'suppression': user_selected_custom_fields,
        'pseudonymisation_HEDNO': user_selected_custom_fields,
        'pseudonymisation_hashing_custom': user_selected_custom_fields,
        'masking_custom': user_selected_custom_fields,  # Enable masking for custom fields
        'pseudonymisation_format_preserving_custom': user_selected_custom_fields
    }

    # Use the global flag to determine if 'generalisation_custom' should be included
    if source == "custom" and generalisation_custom_enabled:
        techniques_mapping_custom['generalisation_custom'] = user_selected_custom_fields
        print("generalisation_custom is enabled")  # Debug print
    else:
        print("generalisation_custom is not enabled")  # Debug print



    # Choose the technique mapping based on the source type
    techniques_mapping = techniques_mapping_custom if source == "custom" else techniques_mapping_general
    # Initialize an empty list to store the applicable techniques
    applicable_techniques = []
    # Iterate over the selected technique mapping dictionary
    for technique, columns in techniques_mapping.items():
        if selected_column in columns:
            applicable_techniques.append(technique)

    return applicable_techniques

def anonymize_column(df, column_name, anonymization_technique, selected1_column):
    #global general_sensitive_columns, general_sensitive_columns_gr
    #all_sensitive_columns = general_sensitive_columns + general_sensitive_columns_gr    
    if anonymization_technique == 'generalisation':
        
            if column_name == "age" or column_name == "ηλικια":
                df = generalize_age(df, selected1_column)
            elif column_name == "address" or column_name == "διευθυνση":
                df = generalize_address(df, selected1_column)
            elif column_name == "post_code" or column_name == "ΤΚ":
                df = generalize_post_code(df, selected1_column)
                print(f"Egine to generalize")
            elif column_name == "salary" or column_name == "μισθος":
                df = generalize_salary(df, selected1_column)
            elif column_name == "payment" or column_name == "πληρωμη":
                df = generalize_payment(df, selected1_column)
            elif column_name == "income" or column_name == "αποδοχες":
                df = generalize_income(df, selected1_column)
            elif column_name == "bill" or column_name == "λογαριασμος":
                df = generalize_bill(df, selected1_column)                
    elif anonymization_technique == 'suppression':
                df = drop_column(df, selected1_column)
    elif anonymization_technique == 'masking':
        # Modify the column data directly in the DataFrame
                df= masking(df, column_name, selected1_column)
    elif anonymization_technique == 'pseudonymisation_substitution':
                df = pseydoanonymization(df, column_name, selected1_column)
    elif anonymization_technique == 'pseudonymisation_format_preserving':
                df = pseydoanonymization(df, column_name, selected1_column)                              
    elif anonymization_technique == 'pseudonymisation_HEDNO':
                df = pseydoanonymization_custom(df, column_name, selected1_column) 
    elif anonymization_technique == 'pseudonymisation_hashing':
                df = pseydoanonymization_hashing(df, column_name, selected1_column)
    elif anonymization_technique == 'pseudonymisation_hashing_custom':
                df = pseydoanonymization_hashing(df, column_name, selected1_column)  
    elif anonymization_technique == 'masking_custom':
                df = mask_custom_field(df, column_name, selected1_column) 
    elif anonymization_technique == 'generalisation_custom':
                df = generalisation_custom_field(df, column_name, selected1_column)   
    elif anonymization_technique == 'pseudonymisation_format_preserving_custom':
                df = pseudonymisation_format_preserving_custom(df, column_name, selected1_column)                                                          
    else:
        # No change needed for other techniques
        pass
    return df