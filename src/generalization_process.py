# Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 
# SPDX-License-Identifier: MIT
# 
# Contributors:
#     Nikolaos Tepelidis - Author
#     Vasileios Siopidis - Couthor
#     Konstantinos Votis - Couthor

import random
import re
def generalize_age(df, selected_column):
    # Convert selected column to integer if it's not already
    df[selected_column] = df[selected_column].astype(int)
    
    # Apply generalization logic based on age ranges
    df[selected_column] = df[selected_column].apply(lambda x: "0-20" if x <= 20 else
                                                       ("21-30" if x <= 30 else
                                                        ("31-40" if x <= 40 else
                                                         ("41-50" if x <= 50 else
                                                          ("51-60" if x <= 60 else
                                                           ">60")))))
    return df

def generalize_address(df, selected_column):
    df[selected_column] = df[selected_column].apply(lambda x: re.sub(r'\d+\s*', '', str(x)).strip())
    return df

def generalize_post_code(df, selected_column):
    # Convert selected column to string if it's not already
    df[selected_column] = df[selected_column].astype(str)
    
    # Define a lambda function to create ranges based on the last three digits
    create_range = lambda x: f"{x[:2]}{(int(x[2:]) - (int(x[2:]) % 100)):03d}-{x[:2]}{(int(x[2:]) - (int(x[2:]) % 100) + 100):03d}"
    
    # Apply the lambda function to create ranges based on the last three digits
    df[selected_column] = df[selected_column].apply(create_range)
    
    return df

def generalize_salary(df, selected_column):
    # Convert selected column to integer if it's not already
    df[selected_column] = df[selected_column].astype(int)
    
    # Apply generalization logic based on age ranges
    df[selected_column] = df[selected_column].apply(lambda x: "0-1000" if x <= 1000 else
                                                       ("1001-2000" if x <= 2000 else
                                                        ("2001-3000" if x <= 3000 else
                                                         ("3001-4000" if x <= 4000 else
                                                          ("4001-9999" if x < 10000 else
                                                            ("10000-20000" if x <= 20000 else
                                                             ("20001-30000" if x <= 30000 else
                                                              ("30001-40000" if x <= 40000 else
                                                               ("40001-50000" if x <= 50000 else
                                                                ("50001-75000" if x <= 75000 else
                                                                 ("75001-100000" if x <= 100000 else
                                                                    ">100000")))))))))))
    return df

def generalize_payment(df, selected_column):

    # Convert selected column to integer if it's not already
    df[selected_column] = df[selected_column].astype(int)
    
    # Apply generalization logic based on age ranges
    df[selected_column] = df[selected_column].apply(lambda x: "0-50" if x <= 50 else
                                                       ("51-100" if x <= 100 else
                                                        ("101-300" if x <= 300 else
                                                         ("301-500" if x <= 500 else
                                                          ("501-750" if x < 750 else
                                                            ("751-1000" if x <= 1000 else
                                                             ("1001-1500" if x <= 1500 else
                                                              ("1501-2000" if x <= 2000 else
                                                               ("2001-5000" if x <= 5000 else
                                                                ("5001-10000" if x <= 10000 else
                                                                 ("10001-50000" if x <= 50000 else
                                                                    ">50000")))))))))))
    return df  

def generalize_income(df, selected_column):  

    # Convert selected column to integer if it's not already
    df[selected_column] = df[selected_column].astype(int)
    
    # Apply generalization logic based on age ranges
    df[selected_column] = df[selected_column].apply(lambda x: "0-1000" if x <= 1000 else
                                                       ("1001-2000" if x <= 2000 else
                                                        ("2001-3000" if x <= 3000 else
                                                         ("3001-4000" if x <= 4000 else
                                                          ("4001-9999" if x < 10000 else
                                                            ("10000-20000" if x <= 20000 else
                                                             ("20001-30000" if x <= 30000 else
                                                              ("30001-40000" if x <= 40000 else
                                                               ("40001-50000" if x <= 50000 else
                                                                ("50001-75000" if x <= 75000 else
                                                                 ("75001-100000" if x <= 100000 else
                                                                    ">100000")))))))))))
    return df  

def generalize_bill(df, selected_column):
        # Convert selected column to integer if it's not already
    df[selected_column] = df[selected_column].astype(int)
    
    # Apply generalization logic based on age ranges
    df[selected_column] = df[selected_column].apply(lambda x: "0-50" if x <= 50 else
                                                       ("51-100" if x <= 100 else
                                                        ("101-200" if x <= 200 else
                                                         ("201-400" if x <= 400 else
                                                          ("401-750" if x < 750 else
                                                            ("751-1000" if x <= 1000 else
                                                             ("1001-1500" if x <= 1500 else
                                                              ("1501-2000" if x <= 2000 else
                                                               ("2001-5000" if x <= 5000 else
                                                                ("5001-10000" if x <= 10000 else
                                                                 ("10001-50000" if x <= 50000 else
                                                                    ">50000")))))))))))
    return df  


def generalisation_custom_field(df, column_name, selected1_column):
    """ Generalizes numeric values in user-selected custom fields. """
    if selected1_column in df.columns:
        def generalize_value(value):
            if isinstance(value, (int, float)):
                value = int(value)
                if 0 <= value <= 100:
                    lower_bound = (value // 20) * 20
                    upper_bound = lower_bound + 20
                    return f"{lower_bound}-{upper_bound}"
                elif 100 < value <= 1000:
                    lower_bound = (value // 100) * 100
                    upper_bound = lower_bound + 100
                    return f"{lower_bound}-{upper_bound}"
                elif 1000 < value <= 10000:
                    lower_bound = (value // 1000) * 1000
                    upper_bound = lower_bound + 1000
                    return f"{lower_bound}-{upper_bound}"
                elif 10000 < value <= 100000:
                    lower_bound = (value // 10000) * 10000
                    upper_bound = lower_bound + 10000
                    return f"{lower_bound}-{upper_bound}"
                else:
                    return ">100000"
            return value  # Return unchanged if not in range

        df[selected1_column] = df[selected1_column].apply(generalize_value)
    
    return df
