# Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 
# SPDX-License-Identifier: MIT
# 
# Contributors:
#     Nikolaos Tepelidis - Author
#     Vasileios Siopidis - Couthor
#     Konstantinos Votis - Couthor

import re

def masking(df, column_name, selected_column):
    # Apply partial masking only to the "email" column
    if column_name.lower() == "email":
        # Iterate over each email in the column
        df[selected_column] = df[selected_column].apply(lambda x: partial_mask_email(str(x)))
    elif column_name.lower() == "phone_number" or column_name.lower() == "κινητο_τηλεφωνο":
        df[selected_column] = df[selected_column].apply(lambda x: partial_mask_phone(str(x)))
    elif column_name.lower() == "social_security" or column_name.lower() == "ΑΜΚΑ":
        df[selected_column] = df[selected_column].apply(lambda x: partial_mask_phone(str(x)))        
    elif column_name.lower() == "vat" or column_name.lower() == "ΑΦΜ":
        df[selected_column] = df[selected_column].apply(lambda x: partial_mask_vat_number(str(x)))
    elif column_name.lower() == "post_code" or column_name.lower() == "ΤΚ":
        df[selected_column] = df[selected_column].apply(lambda x: partial_mask_post_code(str(x)))
    elif column_name.lower() == "salary" or column_name.lower() == "μισθος":
        df[selected_column] = df[selected_column].apply(lambda x: partial_mask_salary(str(x)))
    elif column_name.lower() == "age" or column_name.lower() == "ηλικια":
        df[selected_column] = df[selected_column].apply(lambda x: partial_mask_age(str(x)))
    elif column_name.lower() == "credit_card" or column_name.lower() == "αριθμος_χρεωστικης_καρτας":    
        df[selected_column] = df[selected_column].apply(lambda x: partial_mask_credit_card(str(x)))        
    elif column_name.lower() == "debit_card" or column_name.lower() == "αριθμος_πιστωτικης_καρτας":    
        df[selected_column] = df[selected_column].apply(lambda x: partial_mask_credit_card(str(x)))  
    elif column_name.lower() == "ip_address":    
        df[selected_column] = df[selected_column].apply(lambda x: mask_ip_address(str(x))) 
    elif column_name.lower() == "mac_address":    
        df[selected_column] = df[selected_column].apply(lambda x: mask_mac_address(str(x)))   
    elif column_name.lower() == "bank_account" or column_name.lower() == "αριθμος_λογαριασμου":    
        df[selected_column] = df[selected_column].apply(lambda x: mask_bank_account(str(x))) 
    elif column_name.lower() == "iban":    
        df[selected_column] = df[selected_column].apply(lambda x: mask_iban(str(x))) 
    elif column_name.lower() == "id" or column_name.lower() == "αριθμος_ταυτοτητας":    
        df[selected_column] = df[selected_column].apply(lambda x: mask_id(str(x))) 
    elif column_name.lower() == "passport_number" or column_name.lower() == "αριθμος_διαβατηριου":    
        df[selected_column] = df[selected_column].apply(lambda x: mask_passport_number(str(x))) 
    elif column_name.lower() == "username" or column_name.lower() == "χρηστης":    
        df[selected_column] = df[selected_column].apply(lambda x: mask_username(str(x)))                                       
    return df

def partial_mask_email(email):
    # Split the email into username and domain parts
    parts = email.split('@')
    if len(parts) == 2:
        # Remove spaces from the username
        username = parts[0].replace(' ', '')
        
        # If the username has more than 2 characters, mask the middle ones
        if len(username) > 2:
            masked_username = username[0] + '*' * (len(username) - 2) + username[-1]
        else:
            # If the username has 2 or fewer characters, show them all
            masked_username = username
        
        return masked_username + '@' + parts[1]
    else:
        return email
    
def partial_mask_phone(phone_number):
    # Find all characters that are not digits or '+'
    non_digit_parts = re.findall(r'[^\d+]', phone_number)

    # Extract only the digits and the '+' sign
    clean_number = re.sub(r'[^\d+]', '', phone_number)

    # Check if the cleaned phone number is in the correct format
    if clean_number.isdigit() or (clean_number.startswith('+') and clean_number[1:].isdigit()):
        # Mask the digits: keep the first 3 digits, mask the middle part, and keep the last 3 digits
        masked_number = clean_number[:3] + '*' * (len(clean_number) - 6) + clean_number[-3:]

        # Re-insert the non-digit characters in their original positions
        index = 0
        result = ""
        for char in phone_number:
            if char.isdigit() or char == '+':
                result += masked_number[index]
                index += 1
            else:
                result += char

        return result
    else:
        return phone_number
    
def partial_mask_vat_number(vat_number):
    # Keep the first 2 digits, mask the rest, and keep the last 2 digits
    return vat_number[:2] + '*' * (len(vat_number) - 4) + vat_number[-2:]

def partial_mask_post_code(post_code):
    # Keep only the first 2 digits of the post code
    return post_code[:2] + '*' * (len(post_code) - 2)

def partial_mask_salary(salary):
    # Keep the first 2 digits, mask the rest, and add '***'
    return salary[:1] + '*****'

def partial_mask_age(age):
    # Keep only the first digit of the age
    return age[0] + '*' * (len(age) - 1)

def partial_mask_credit_card(credit_card_number):
    # Check if the credit card number contains hyphens
    if '-' in credit_card_number:
        # Remove hyphens
        cleaned_number = credit_card_number.replace('-', '')
        # Mask the cleaned number
        if len(cleaned_number) > 8:
            masked = cleaned_number[:4] + '*' * (len(cleaned_number) - 8) + cleaned_number[-4:]
        else:
            masked = cleaned_number
        # Reinsert hyphens
        return '-'.join([masked[i:i+4] for i in range(0, len(masked), 4)])
    else:
        # Mask without hyphens
        if len(credit_card_number) > 8:
            return credit_card_number[:4] + '*' * (len(credit_card_number) - 8) + credit_card_number[-4:]
        else:
            return credit_card_number
        
        
def mask_ip_address(ip_address):
    # Keep the first 3 digits, mask the rest, and keep the last 2 digits
    return ip_address[:3] + '*' * (len(ip_address) - 4) + ip_address[-2:]

def mask_mac_address(mac_address):
    # Keep the first 3 digits, mask the rest, and keep the last 2 digits
    return mac_address[:3] + '*' * (len(mac_address) - 4) + mac_address[-2:]

def mask_bank_account(bank_account):
    # Keep the first 3 digits, mask the rest, and keep the last 2 digits
    return bank_account[:3] + '*' * (len(bank_account) - 4) + bank_account[-2:]
def mask_iban(iban):
    # Keep the first 6 digits, mask the rest, and keep the last 4 digits
    return iban[:6] + '*' * (len(iban) - 4) + iban[-4:]
def mask_id(id):
    # Keep the first 2 digits, mask the rest, and keep the last 2 digits
    return id[:2] + '*' * (len(id) - 4) + id[-2:]
def mask_passport_number(passport_number):
    # Keep the first 2 digits, mask the rest, and keep the last 2 digits
    return passport_number[:2] + '*' * (len(passport_number) - 4) + passport_number[-2:]
def mask_username(username):
    # Keep the first 1 digits, mask the rest, and keep the last 1 digits
    return username[:1] + '*' * (len(username) - 4) + username[-1:]


def mask_custom_field(df, column_name, selected1_column):
    """ 
    Mask words in a DataFrame column based on length.
    
    - Words ≤ 7 chars → Keep **first & last letter**, replace middle with `*`.
    - Words > 7 chars → Keep **first 2 & last 2 letters**, replace middle with `*`.
    """

    def mask_value(value):
        """ Apply masking to a single value. """
        if not isinstance(value, str):  # Ensure it's a string
            return value

        words = value.split()  # Split text into words
        masked_words = []

        for word in words:
            if len(word) <= 7:
                masked_word = word[0] + '*' * (len(word) - 2) + word[-1] if len(word) > 2 else word
            else:
                masked_word = word[:2] + '*' * (len(word) - 4) + word[-2:]
            masked_words.append(masked_word)

        return ' '.join(masked_words)  # Reassemble the text

    # Apply the masking function to the specified column
    df[selected1_column] = df[selected1_column].apply(mask_value)

    return df