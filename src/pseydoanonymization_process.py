# Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 
# SPDX-License-Identifier: MIT
# 
# Contributors:
#     Nikolaos Tepelidis - Author
#     Vasileios Siopidis - Couthor
#     Konstantinos Votis - Couthor

import faker
import hashlib
import random
import string
from faker import Faker
import unicodedata


def pseydoanonymization(df, column_name, selected_column):
    # Initialize a Faker generator
    fake = faker.Faker()
    value_map = {}  # Dictionary to map original values to fake values
    credit_card_counter = 1  
        # Predefine the list of credit card labels
    #credit_card_labels = [f"credit card {i}" for i in range(1, 10000000)]  

    # Function to get or generate a fake value
    def get_fake_value(original_value, fake_func):
        if original_value not in value_map:
            value_map[original_value] = fake_func()
        return value_map[original_value]
    
    # Custom function to generate numeric phone numbers
    def fake_numeric_phone_number():
        return fake.numerify(text="##########")  # Generates a 10-digit numeric phone number
    

            # Detect gender using the first name
        detected_gender = gender_detector.get_gender(name_parts[0])
    # Function to pseudonymize names with gender consideration
    def pseudonymize_name(value):
        words = value.split()

        if len(words) == 0:
            return value  # Return unchanged if empty

        if len(words) == 1:  # If the value contains only one word
            if is_greek(words[0]):  # Greek single name
                return random.choice(greek_names_males + greek_names_females)
            else:  # English single name
                return fake.first_name()
        
        elif len(words) > 1:  # If the value contains multiple words (name + surname)
            if is_greek(words[0]):  # Greek names
                if words[0] in greek_names_males:
                    return f"{random.choice(greek_names_males)} {random.choice(male_surnames)}"
                elif words[0] in greek_names_females:
                    return f"{random.choice(greek_names_females)} {random.choice(female_surnames)}"
                else:
                    # Randomly pick either male or female name with matching surname
                    if random.choice([True, False]):  # Randomly pick male or female
                        return f"{random.choice(greek_names_males)} {random.choice(male_surnames)}"
                    else:
                        return f"{random.choice(greek_names_females)} {random.choice(female_surnames)}"
            else:  # English names
                return f"{fake.first_name()} {fake.last_name()}"
    
    # Custom function for passport numbers (2 letters followed by 8 digits)
    def fake_passport_number():
        return fake.random_uppercase_letter() + fake.random_uppercase_letter() + fake.numerify(text="########")    

    # Custom function to generate a fake IP address
    def fake_ip_address():
        return f"{fake.random_int(min=1, max=255)}.{fake.random_int(min=0, max=255)}.{fake.random_int(min=0, max=255)}.{fake.random_int(min=0, max=255)}"

    # Custom function to generate a fake MAC address
    def fake_mac_address():
        return ":".join(f"{fake.random_int(min=0, max=255):02x}" for _ in range(6))  

    # Custom function to generate a fake bank account number
    def fake_bank_account():
        return f"{fake.random_int(min=100, max=999)}-{fake.random_int(min=1000, max=9999)}-{fake.random_int(min=100, max=999)}" 

    # Custom function for passport numbers (2 letters followed by 8 digits)
    def fake_iban():
        return fake.random_uppercase_letter() + fake.random_uppercase_letter() + fake.numerify(text="##################")
    
    def fake_credit_card():
        return ''.join(str(random.randint(0, 9)) for _ in range(16))
    
    def format_credit_card(original_cc, fake_cc):
        if " " in original_cc:
            return ' '.join(fake_cc[i:i+4] for i in range(0, 16, 4))
        elif "-" in original_cc:
            return '-'.join(fake_cc[i:i+4] for i in range(0, 16, 4))
        else:
            return fake_cc

    # List of fake ethnicities
    fake_ethnicities = [
        "Caucasian",
        "Asian",
        "African",
        "Hispanic",
        "Middle Eastern",
        "Native American",
        "Pacific Islander",
        "Mixed Race",
        "Caribbean",
        "South Asian",
        "East Asian",
        "European",
        "Latino",
        "Arab",
        "Aboriginal",
        "Indigenous Australian",
    ]   

    fake_religions = [
        "Christianity",
        "Islam",
        "Hinduism",
        "Buddhism",
        "Judaism",
        "Sikhism",
        "Taoism",
        "Shinto",
        "Zoroastrianism",
        "Jainism",
        "Confucianism",
        "Scientology",
        "Rastafarianism",

    ]


    # Generate fake data for the selected column
    if column_name.lower() == "name" or column_name.lower() == "ονομα":
        df[selected_column] = df[selected_column].apply(lambda x: get_fake_value(x, lambda: pseudonymize_name(x)))
    elif column_name.lower() == "social_security" or column_name.lower() == "ΑΜΚΑ":
        df[selected_column] = df[selected_column].apply(lambda x: get_fake_value(x, fake_numeric_phone_number))      
    elif column_name.lower() == "surname" or column_name.lower() == "επιθετο":
        df[selected_column] = df[selected_column].apply(lambda x: get_fake_value(x, fake.last_name))
    elif column_name.lower() == "age" or column_name.lower() == "ηλικια":
        df[selected_column] = df[selected_column].apply(lambda x: get_fake_value(x, lambda: fake.random_int(min=18, max=90)))
    elif column_name.lower() == "post_code" or column_name.lower() == "ΤΚ":
        df[selected_column] = df[selected_column].apply(lambda x: get_fake_value(x, fake.postcode))
    elif column_name.lower() == "address" or column_name.lower() == "διευθυνση":
        df[selected_column] = df[selected_column].apply(lambda x: get_fake_value(x, fake.street_address))
    elif column_name.lower() == "town" or column_name.lower() == "πολη":
        df[selected_column] = df[selected_column].apply(lambda x: get_fake_value(x, fake.city))
    elif column_name.lower() == "country" or column_name.lower() == "χωρα":
        df[selected_column] = df[selected_column].apply(lambda x: get_fake_value(x, fake.country))
    elif column_name.lower() == "phone_number" or column_name.lower() == "κινητο_τηλεφωνο":
        df[selected_column] = df[selected_column].apply(lambda x: get_fake_value(x, fake_numeric_phone_number))  # Use custom numeric phone number generator
    elif column_name.lower() == "email":
        df[selected_column] = df[selected_column].apply(lambda x: get_fake_value(x, fake.email))
    elif column_name.lower() == "vat" or column_name.lower() == "ΑΦΜ":
        df[selected_column] = df[selected_column].apply(lambda x: get_fake_value(x, lambda: fake.random_int(min=100000000, max=999999999)))
    elif column_name.lower() == "gender" or column_name.lower() == "φυλο":
        df[selected_column] = df[selected_column].apply(lambda x: get_fake_value(x, lambda: fake.random_element(elements=('Male', 'Female'))))
    elif column_name.lower() == "salary" or column_name.lower() == "μισθος":
        df[selected_column] = df[selected_column].apply(lambda x: get_fake_value(x, lambda: fake.random_int(min=2000, max=100000)))
    elif column_name.lower() == "credit_card" or column_name.lower() == "αριθμος_χρεωστικης_καρτας":

        df[selected_column] = df[selected_column].astype(str).apply(lambda x: format_credit_card(x, get_fake_value(x, fake_credit_card)))
    elif column_name.lower() == "debit_card" or column_name.lower() == "αριθμος_πιστωτικης_καρτας":

        df[selected_column] = df[selected_column].astype(str).apply(lambda x: format_credit_card(x, get_fake_value(x, fake_credit_card)))  

    elif column_name.lower() == "passport_number" or column_name.lower() == "αριθμος_διαβατηριου":
        df[selected_column] = df[selected_column].apply(lambda x: get_fake_value(x, fake_passport_number))
    elif column_name.lower() == "id" or column_name.lower() == "αριθμος_ταυτοτητας":
        df[selected_column] = df[selected_column].apply(lambda x: get_fake_value(x, fake_passport_number))
    elif column_name.lower() == "ip_address":
        df[selected_column] = df[selected_column].apply(lambda x: get_fake_value(x, fake_ip_address))
    elif column_name.lower() == "mac_address":
        df[selected_column] = df[selected_column].apply(lambda x: get_fake_value(x, fake_mac_address))                              
    elif column_name.lower() == "bank_account" or column_name.lower() == "αριθμος_λογαριασμου":
        df[selected_column] = df[selected_column].apply(lambda x: get_fake_value(x, fake_bank_account))
    elif column_name.lower() == "iban": 
        df[selected_column] = df[selected_column].apply(lambda x: get_fake_value(x, fake_iban))
    elif column_name.lower() == "income" or column_name.lower() == "αποδοχες":
        df[selected_column] = df[selected_column].apply(lambda x: get_fake_value(x, lambda: fake.random_int(min=1000, max=10000)))
    elif column_name.lower() == "ethnicity" or column_name.lower() == "εθνικοτητα":
        df[selected_column] = df[selected_column].apply(lambda x: get_fake_value(x, lambda: fake.random_element(elements=fake_ethnicities)))
    elif column_name.lower() == "religion" or column_name.lower() == "θρησκευμα": 
        df[selected_column] = df[selected_column].apply(lambda x: get_fake_value(x, lambda: fake.random_element(elements=fake_religions)))
    elif column_name.lower() == "customer" or column_name.lower() == "πελατης":
        df[selected_column] = df[selected_column].apply(lambda x: get_fake_value(x, lambda: pseudonymize_name(x)))
    elif column_name.lower() == "supplier" or column_name.lower() == "προμηθευτης":
        df[selected_column] = df[selected_column].apply(lambda x: get_fake_value(x, lambda: pseudonymize_name(x)))
        #df[selected_column] = df[selected_column].apply(lambda x: pseudonymize_name(x))  #If i dont want the same fake values for multiple same original values
        
    return df

#column name= column on database, selected1_column= column of the dataset
def pseydoanonymization_custom(df, column_name, selected1_column):
    """
    Apply pseudoanonymization for a custom column. Replace each unique value in the column
    with a string in the format '<column_name>-<unique_number>'.
    """
    # Dictionary to store the mapping of original values to pseudoanonymized values
    value_to_pseudo_map = {}
    counter = 1  # Initialize counter for numbering

    # Iterate through each value in the column
    for index, value in df[selected1_column].items():
        if value not in value_to_pseudo_map:
            # Create a new pseudoanonymized value for unique entries
            value_to_pseudo_map[value] = f"{selected1_column}-{counter}"
            counter += 1
        # Replace the value in the DataFrame with its pseudoanonymized equivalent
        df.at[index, selected1_column] = value_to_pseudo_map[value]
    
    return df


def pseydoanonymization_hashing(df, column_name, selected_column):
    # Dictionary to map original values to hashed values
    value_map = {}

    # Function to get or generate a hashed value
    def get_hashed_value(original_value):
        if original_value not in value_map:
            # Hash the value using SHA-256
            hash_object = hashlib.sha256(str(original_value).encode())
            value_map[original_value] = hash_object.hexdigest()
        return value_map[original_value]

    # Hash the column data for the selected column
    df[selected_column] = df[selected_column].apply(lambda x: get_hashed_value(x))
    return df

fake = Faker()
fake_first_names = {fake.first_name().lower() for _ in range(50000)} # Create a set of fake first names


greek_names_males = [
    "Αλέξανδρος", "Γεώργιος", "Δημήτριος", "Ιωάννης", "Κωνσταντίνος", "Νικόλαος", "Παναγιώτης", "Στυλιανός",
    "Χαράλαμπος", "Ανδρέας", "Θεόδωρος", "Χρήστος", "Ευάγγελος", "Σωτήρης", "Αριστείδης", "Λεωνίδας",
    "Μιχαήλ", "Ελευθέριος", "Ηλίας", "Πέτρος", "Αθανάσιος", "Βασίλειος", "Σπυρίδων", "Μάρκος", "Άγγελος",
    "Μάριος", "Χριστόφορος", "Φίλιππος", "Σαράντης", "Ευστράτιος", "Πολυχρόνης", "Νέστορας", "Αντώνιος",
    "Σέργιος", "Ορέστης", "Λουκάς", "Νικηφόρος", "Αλκιβιάδης", "Προκόπιος", "Αλέξης", "Τιμόθεος", "Παύλος",
    "Ραφαήλ", "Μανώλης", "Επαμεινώνδας", "Αριστοτέλης", "Περικλής", "Ζαχαρίας", "Βίκτωρ", "Διονύσιος",
    "Εμμανουήλ", "Σεραφείμ", "Κλεάνθης", "Στρατής", "Ιάκωβος", "Λάζαρος", "Ανάργυρος", "Ευσέβιος", "Ναπολέων",
    "Αμβρόσιος", "Δαμιανός", "Τριαντάφυλλος", "Εφραίμ", "Ξενοφών", "Θρασύβουλος", "Ισίδωρος"
]

greek_names_females = [    
    "Ειρήνη", "Μαρία", "Αναστασία", "Ελένη", "Κατερίνα", "Σοφία", "Αγγελική", "Βασιλική", "Χριστίνα", "Γεωργία",
    "Θεοδώρα", "Νικολέτα", "Δέσποινα", "Παρασκευή", "Ευαγγελία", "Ευγενία", "Πηνελόπη", "Δωροθέα", "Ελπίδα",
    "Αικατερίνη", "Κυριακή", "Χαρίκλεια", "Αναστασία", "Ολυμπία", "Σταυρούλα", "Ιουλία", "Στυλιανή", "Αργυρώ",
    "Ζωή", "Ευφροσύνη", "Αθηνά", "Διαμαντία", "Χλόη", "Αριάδνη", "Νεφέλη", "Λυδία", "Ιφιγένεια", "Αλεξάνδρα",
    "Ροδοθέα", "Ευτυχία", "Ισμήνη", "Μελίνα", "Μυρτώ", "Ερατώ", "Φωτεινή", "Κλεοπάτρα", "Ρέα", "Αφροδίτη",
    "Αυγή", "Ινώ", "Δανάη", "Φαίδρα", "Ναυσικά", "Μαργαρίτα", "Πολυξένη", "Αντιγόνη", "Εριφύλη", "Χρυσαυγή",
    "Χρυσούλα", "Αλεξία", "Γλυκερία", "Δήμητρα", "Ευσταθία", "Λαμπρινή", "Ουρανία", "Πολύμνια", "Καλλιόπη",
    "Τερψιχόρη", "Θάλεια", "Ευτέρπη", "Κλειώ", "Ερατώ", "Καλλιστώ", "Θεανώ", "Χρυσάνθη", "Ροδή", "Ευανθία",
    "Λεμονιά", "Κοραλία", "Δήμητρα", "Αργυρή", "Ελβίρα", "Ασπασία", "Ματθίλδη", "Ευμορφία", "Νίκη", "Αγάπη",
    "Αναξαγόρας", "Ανδρονίκη", "Βερονίκη", "Διοτίμα", "Ευδοκία", "Καλλισθένη", "Μερόπη", "Πανδώρα", "Ρωξάνη",
    "Σαπφώ", "Θεοφανία", "Λεοντία", "Ολυμπιάδα", "Συντυχία", "Φιλοθέη", "Χρυσή", "Άννα", "Κασσιανή", "Σμαράγδα",
    "Ευλαμπία", "Ιλαρία", "Αμβροσία", "Θεοφανώ", "Καρμέλα", "Μαλαματένια", "Φρειδερίκη", "Ανδρομάχη",
    "Πανωραία", "Πηνελόπη", "Πασχαλία", "Ελεονώρα", "Αριάνα", "Ντάρια", "Θεονύμφη", "Αγνοδίκη", "Δήμητρα",
    "Κλεονίκη", "Χαρίκλεια", "Ιουστίνα", "Μηλίτσα", "Φιλομήλα", "Αικατερίνη", "Βεατρίκη", "Νικολέττα"
]

foreign_names = [
    "James", "John", "Robert", "Michael", "William", "David", "Richard", "Joseph", "Thomas", "Charles",
    "Christopher", "Daniel", "Matthew", "Anthony", "Donald", "Mark", "Paul", "Steven", "Andrew", "Kenneth",
    "George", "Joshua", "Kevin", "Brian", "Edward", "Ronald", "Timothy", "Jason", "Jeffrey", "Ryan",
    "Jacob", "Gary", "Nicholas", "Eric", "Stephen", "Jonathan", "Larry", "Justin", "Scott", "Brandon",
    "Benjamin", "Samuel", "Gregory", "Frank", "Alexander", "Raymond", "Patrick", "Jack", "Dennis", "Jerry",
    "Tyler", "Aaron", "Jose", "Adam", "Nathan", "Henry", "Zachary", "Walter", "Kyle", "Harold",
    "Carl", "Jeremy", "Gerald", "Ethan", "Arthur", "Lawrence", "Jordan", "Jesse", "Louis", "Bryan",
    "Billy", "Bruce", "Noah", "Dylan", "Alan", "Ralph", "Gabriel", "Roy", "Juan", "Wayne",
    "Eugene", "Logan", "Randy", "Vincent", "Philip", "Bobby", "Johnny", "Bradley", "Mary", "Patricia",
    "Jennifer", "Linda", "Elizabeth", "Barbara", "Susan", "Jessica", "Sarah", "Karen", "Nancy", "Lisa",
    "Margaret", "Betty", "Sandra", "Ashley", "Dorothy", "Kimberly", "Emily", "Donna", "Michelle", "Carol",
    "Amanda", "Melissa", "Deborah", "Stephanie", "Rebecca", "Laura", "Sharon", "Cynthia", "Kathleen", "Amy",
    "Shirley", "Angela", "Helen", "Anna", "Brenda", "Pamela", "Nicole", "Samantha", "Katherine", "Emma",
    "Christine", "Debra", "Rachel", "Catherine", "Carolyn", "Janet", "Ruth", "Maria", "Heather", "Diane",
    "Virginia", "Julie", "Joyce", "Victoria", "Olivia", "Kelly", "Christina", "Lauren", "Joan", "Evelyn",
    "Judith", "Megan", "Andrea", "Cheryl", "Hannah", "Jacqueline", "Martha", "Gloria", "Teresa", "Ann",
    "Sara", "Madison", "Kathryn", "Savannah", "Alyssa", "Janice", "Tiffany", "Jean", "Alice", "Judy",
    "Abigail", "Alexis", "Grace", "Marilyn", "Denise", "Amber", "Danielle", "Brittany", "Diana", "Natalie",
    "Sophia", "Rose", "Isabella", "Theresa", "Charlotte", "Marie", "Kayla", "Carmen", "Joanne", "Gail",
    "Lucy", "Eleanor", "Ellen", "Erin", "Stacy", "Nora", "Clara", "Melanie", "Veronica", "Felicia"
]


# Λίστα με 100 αντρικά ελληνικά επίθετα
male_surnames = [
    "Παπαδόπουλος", "Παπαδάκης", "Πετρόπουλος", "Νικολάου", "Γεωργίου", "Στεφανίδης", "Αναγνωστόπουλος",
    "Καραγιάννης", "Δημητριάδης", "Αλεξίου", "Βασιλείου", "Χριστοδούλου", "Ιωαννίδης", "Σαββίδης",
    "Μιχαηλίδης", "Αντωνίου", "Λαμπρόπουλος", "Σπυρόπουλος", "Μανωλάκης", "Θεοδωρίδης", "Αποστόλου",
    "Κωνσταντίνου", "Καραμανλής", "Σταματόπουλος", "Οικονόμου", "Παπαγεωργίου", "Λυμπερόπουλος",
    "Κυριακίδης", "Τριανταφυλλίδης", "Σταθόπουλος", "Ρουμπέσης", "Χατζηγεωργίου", "Ζαχαριάδης",
    "Παναγιωτόπουλος", "Καρράς", "Σωτηρόπουλος", "Κυριαζής", "Διαμαντόπουλος", "Αργυρόπουλος",
    "Ελευθεριάδης", "Σιδηρόπουλος", "Κουτσογιάννης", "Ανδρεόπουλος", "Χαραλαμπίδης", "Δημόπουλος",
    "Αλεξανδρίδης", "Λυκούργος", "Γρηγοριάδης", "Μαυρομάτης", "Παναγής", "Φωτόπουλος", "Ξανθόπουλος",
    "Κοκκινίδης", "Γαλανόπουλος", "Δασκαλάκης", "Λεμονής", "Παπαδάτος", "Στυλιανού", "Βουλγαρίδης",
    "Κορωνιός", "Χαλκιάς", "Μανωλάκος", "Αρβανίτης", "Στρατής", "Κουλούρης", "Καπετάνος",
    "Μαυρογενής", "Λαγός", "Καλογερόπουλος", "Καρατζάς", "Καρβούνης", "Πολίτης", "Λιακόπουλος",
    "Τσακίρης", "Αποστολίδης", "Καλογιάννης", "Ράπτης", "Κορδέλας", "Κομνηνός", "Δεληγιάννης",
    "Μπουρλάς", "Τσάκος", "Βεργίνης", "Μπαξεβάνης", "Παπακωνσταντίνου", "Μπάρκας", "Ζερβός",
    "Νικολόπουλος", "Κουτσουμπός", "Αναγνωστάκης", "Κουτσός", "Καλαμάρης", "Χρονόπουλος",
    "Πετρίδης", "Σούλης", "Χατζόπουλος", "Κατσιφής", "Δούκας", "Σμυρνής", "Γουλιμής"
]

# Λίστα με 100 γυναικεία ελληνικά επίθετα
female_surnames = [
    "Παπαδοπούλου", "Παπαδάκη", "Πετρόπουλου", "Νικολάου", "Γεωργίου", "Στεφανίδου", "Αναγνωστοπούλου",
    "Καραγιάννη", "Δημητριάδη", "Αλεξίου", "Βασιλείου", "Χριστοδούλου", "Ιωαννίδη", "Σαββίδη",
    "Μιχαηλίδη", "Αντωνίου", "Λαμπροπούλου", "Σπυροπούλου", "Μανωλάκη", "Θεοδωρίδη", "Αποστόλου",
    "Κωνσταντίνου", "Καραμανλή", "Σταματοπούλου", "Οικονόμου", "Παπαγεωργίου", "Λυμπεροπούλου",
    "Κυριακίδου", "Τριανταφυλλίδου", "Σταθοπούλου", "Ρουμπέση", "Χατζηγεωργίου", "Ζαχαριάδη",
    "Παναγιωτοπούλου", "Καρά", "Σωτηροπούλου", "Κυριαζή", "Διαμαντοπούλου", "Αργυροπούλου",
    "Ελευθεριάδη", "Σιδηροπούλου", "Κουτσογιάννη", "Ανδρεοπούλου", "Χαραλαμπίδου", "Δημοπούλου",
    "Αλεξανδρίδου", "Λυκούργου", "Γρηγοριάδη", "Μαυρομάτη", "Παναγή", "Φωτοπούλου", "Ξανθοπούλου",
    "Κοκκινίδου", "Γαλανόπουλου", "Δασκαλάκη", "Λεμονή", "Παπαδάτου", "Στυλιανού", "Βουλγαρίδου",
    "Κορωνιού", "Χαλκιά", "Μανωλάκου", "Αρβανίτη", "Στρατή", "Κουλούρη", "Καπετάνου",
    "Μαυρογενή", "Λαγού", "Καλογεροπούλου", "Καρατζά", "Καρβούνη", "Πολίτη", "Λιακοπούλου",
    "Τσακίρη", "Αποστολίδη", "Καλογιάννη", "Ράπτη", "Κορδέλα", "Κομνηνού", "Δεληγιάννη",
    "Μπουρλά", "Τσάκου", "Βεργίνη", "Μπαξεβάνη", "Παπακωνσταντίνου", "Μπάρκα", "Ζερβού",
    "Νικολοπούλου", "Κουτσουμπού", "Αναγνωστάκη", "Κουτσού", "Καλαμάρη", "Χρονοπούλου",
    "Πετρίδου", "Σούλη", "Χατζοπούλου", "Κατσιφή", "Δούκα", "Σμυρνή", "Γουλιμή"
]

def contains_name(column_values):
    """
    Checks if at least one value in the column contains a known first name (case insensitive).
    """
    # Convert name lists to lowercase sets for faster lookup
    greek_names_males_lower = {name.lower() for name in greek_names_males}
    greek_names_females_lower = {name.lower() for name in greek_names_females}
    foreign_names_lower = {name.lower() for name in foreign_names}

    for value in column_values:
        words = value.split()
        for word in words:
            word_lower = word.lower()  # Convert word to lowercase
            
            if (word_lower in greek_names_males_lower or 
                word_lower in greek_names_females_lower or 
                word_lower in foreign_names_lower):
                print(f"Detected name: {word} in {value}")  # Debugging
                return True  # Found a name, return immediately
            
    return False

def is_greek(text):
    """Check if the given text is written in Greek."""
    for char in text:
        if 'GREEK' in unicodedata.name(char, ''):
            return True
    return False

def generate_fake_value_custom_format_preserving(value, always_replace=False):
    """
    Generates fake names:
    - If always_replace is True, the entire value will be replaced with a fake name or name + surname.
    - If the value contains a recognized name, it's replaced with a random first name.
    - If a surname follows the first name, it's replaced with a fake surname.
    - Otherwise, non-name values are randomized while preserving structure.
    """
    words = value.split()
    
    if always_replace:


        # Preserve the structure of names (one word → one name, two words → name + surname)
        if len(words) == 1:  # If the value contains only one word
            if is_greek(words[0]):  # Greek single name
                return random.choice(greek_names_males + greek_names_females)
            else:  # English single name
                return fake.first_name()
        elif len(words) > 1:  # If the value contains two words
            if is_greek(words[0]):  # Greek names
                if words[0] in greek_names_males:
                    return f"{random.choice(greek_names_males)} {random.choice(male_surnames)}"
                elif words[0] in greek_names_females:
                    return f"{random.choice(greek_names_females)} {random.choice(female_surnames)}"
                else:
                    # Randomly pick either male or female name with matching surname
                    if random.choice([True, False]):  # Randomly pick male or female
                        return f"{random.choice(greek_names_males)} {random.choice(male_surnames)}"
                    else:
                        return f"{random.choice(greek_names_females)} {random.choice(female_surnames)}"
            else:  # English names
                    return f"{fake.first_name()} {fake.last_name()}" 


    fake_value = []
    previous_was_name = False

    for word in words:
        if word.lower in greek_names_males:  # Check if the word is in the Greek names list
            fake_value.append(random.choice(greek_names_males))  # Replace with a random Greek name
            previous_was_name = True
            if previous_was_name:
                fake_value.append(random.choice(male_surnames)) 
                previous_was_name = False
        elif word.lower in greek_names_females:  # Check if the word is in the Greek names list
            fake_value.append(random.choice(greek_names_females))  # Replace with a random Greek name
            if previous_was_name:
                fake_value.append(random.choice(female_surnames))     
                previous_was_name = False        
        elif word.lower() in foreign_names:
            fake_value.append(fake.first_name())
            previous_was_name = True
        elif previous_was_name:
            fake_value.append(fake.last_name())
            previous_was_name = False
        elif word.isdigit():  # If the word is completely numeric
            fake_value.append("".join(random.choices(string.digits, k=len(word))))  # Ensure same length            
        else:
            transformed_word = []
            for char in word:
                if char.isalpha():
                    transformed_word.append(random.choice(string.ascii_uppercase))
                elif char.isdigit():
                    transformed_word.append(random.choice(string.digits))
                else:
                    transformed_word.append(char)
            fake_value.append("".join(transformed_word))

    return " ".join(fake_value)

def pseudonymisation_format_preserving_custom(df, column_name, selected1_column):
    """Applies format-preserving pseudonymization to custom fields."""
    if selected1_column in df.columns:
        column_values = df[selected1_column].astype(str).tolist()

        # Check if any row contains a name
        has_names = contains_name(column_values)

        # Apply transformation accordingly
        df[selected1_column] = df[selected1_column].astype(str).apply(
            lambda value: generate_fake_value_custom_format_preserving(value, always_replace=has_names)
        )
    return df

#Implementation without names check 
#def generate_fake_value(value):
#    """
#    Generates a fake value while preserving the format of the original.
#    Letters are replaced with random letters, and digits with random digits.
#    """
#    fake_value = []
#    for char in value:
#        if char.isalpha():
#            fake_value.append(random.choice(string.ascii_uppercase))  # Preserve uppercase format
#        elif char.isdigit():
#            fake_value.append(random.choice(string.digits))  # Preserve digit format
#        else:
#            fake_value.append(char)  # Keep special characters unchanged
#    return "".join(fake_value)

#def pseudonymisation_format_preserving_custom(df, column_name, selected1_column):
#    """Applies format-preserving pseudonymization to custom fields."""
#    if selected1_column in df.columns:
#        df[selected1_column] = df[selected1_column].astype(str).apply(generate_fake_value)
#    return df