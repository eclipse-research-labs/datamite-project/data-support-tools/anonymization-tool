# Copyright (c) 2024 Centre for Research and Technology Hellas (CERTH) / Information Technologies Institute (ITI)
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 
# SPDX-License-Identifier: MIT
# 
# Contributors:
#     Nikolaos Tepelidis - Author
#     Vasileios Siopidis - Couthor
#     Konstantinos Votis - Couthor
from werkzeug.exceptions import RequestEntityTooLarge
import pandas as pd #used for data manipulation and analysis
from flask import Flask, render_template, request, jsonify, make_response, url_for #Flask is a web framework for building web applications. render_template, request, and jsonify are specific components of Flask for rendering templates, handling HTTP requests, and formatting JSON responses, respectively. 
from flask_cors import CORS  # CORS is used for enabling Cross-Origin Resource Sharing, allowing the server to handle requests from different origins
from flask_swagger_ui import get_swaggerui_blueprint
#from anonymization import AnonymizationHandler
import numpy as np
import json
from werkzeug.utils import secure_filename
import nltk
nltk.download('punkt')
from datetime import datetime
import uuid
import sys
from nltk.stem import WordNetLemmatizer
lemmatizer = WordNetLemmatizer()
nltk.download('wordnet')
from nltk.tokenize import word_tokenize
from flask import send_file
from flask_swagger_ui import get_swaggerui_blueprint
import io
from io import BytesIO
import logging
from anonymization_techniques_forcolumn import post_column_info, post_anonymization_info, update_user_selected_custom_fields
import re
import unicodedata
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy import create_engine, MetaData, Table, Column, String
from sqlalchemy.dialects.postgresql import JSONB
engine = None  # Your database engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine, MetaData, Table, select, join, or_, cast, String, JSON, DateTime
from sqlalchemy.sql import func, and_ , or_

# Temporary storage for anonymized data and tokens
temp_storage = {}  # In-memory dictionary for storing anonymized data with tokens
# a Flask application is created, and CORS is enabled for all routes.
# This is necessary if you want to make requests to this server from a different domain.
app = Flask(__name__)
CORS(app)  # Enable CORS for all routes

@app.after_request
def add_cors_headers(response):
    response.headers["Access-Control-Allow-Origin"] = "*"  # Allow all origins
    response.headers["Access-Control-Allow-Methods"] = "GET, POST, PUT, DELETE, OPTIONS"  # Allow these methods
    response.headers["Access-Control-Allow-Headers"] = "Content-Type, Authorization, X-Requested-With"  # Allow these headers
    return response

from difflib import SequenceMatcher
from sqlalchemy import ARRAY
from sqlalchemy import literal

SWAGGER_URL = '/anonymization'  # URL for exposing Swagger UI (without trailing '/')
API_URL = '/static/swaggerfinal.json'  # Our API url (can of course be a local resource)

swaggerui_blueprint = get_swaggerui_blueprint(
    SWAGGER_URL,  # Swagger UI static files will be mapped to '{SWAGGER_URL}/dist/'
    API_URL,
    config={  # Swagger UI config overrides
        'app_name': "Anonymisation"
    },
    # oauth_config={  # OAuth config. See https://github.com/swagger-api/swagger-ui#oauth2-configuration .
    #    'clientId': "your-client-id",
    #    'clientSecret': "your-client-secret-if-required",
    #    'realm': "your-realms",
    #    'appName': "your-app-name",
    #    'scopeSeparator': " ",
    #    'additionalQueryStringParams': {'test': "hello"}
    # }
)

app.register_blueprint(swaggerui_blueprint)

# global variable df is initialized as an empty pandas DataFrame. This variable will be used to store the loaded data.
df = pd.DataFrame()
csv_loaded = False
uploaded_file_format = None
selected_columns = []
sensitive_columns_energy = []
sensitive_columns_telecommunication = []
sensitive_columns_agriculture = []
sensitive_columns_meteo = []
general_sensitive_columns = ["name", "surname", "age", "post_code", "address", "town", "country", "phone_number", "email", "vat", "gender", "salary", "credit_card","debit_card", "social_security", "passport_number", "id", "username", "password", "ip_address", "mac_address", "coordinates", "bank_account", "iban", "payment", "income", "ethnicity", "religion", "customer", "supplier", "bill"]
general_sensitive_columns_gr = ["ονομα", "επιθετο", "ηλικια", "ΤΚ", "διευθυνση", "πολη", "χωρα", "κινητο_τηλεφωνο", "email", "ΑΦΜ", "φυλο", "μισθος", "αριθμος_χρεωστικης_καρτας","αριθμος_πιστωτικης_καρτας", "ΑΜΚΑ", "αριθμος_διαβατηριου", "αριθμος_ταυτοτητας", "χρηστης", "κωδικος_προσβασης", "συντεταγμενες", "αριθμος_λογαριασμου", "πληρωμη", "αποδοχες", "εθνικοτητα", "θρησκευμα", "πελατης", "προμηθευτης", "λογαριασμος"]
sensitive_columns = general_sensitive_columns + sensitive_columns_energy + sensitive_columns_telecommunication + sensitive_columns_agriculture + sensitive_columns_meteo
# Setup logging
logging.basicConfig(level=logging.DEBUG)
# Database connection
#DATABASE_URI = 'postgresql://postgres:1234@localhost:5432/anonymization_tool'
#engine = create_engine(DATABASE_URI)

# Define the database URI
DATABASE_URI = 'postgresql://postgres:1234@postgres:5432/anonymization_tool'
anonymized_data =None
try:
    # Create the database engine
    engine = create_engine(DATABASE_URI)
    factory = sessionmaker(bind=engine)
    session = factory()
    
    # Test the connection by attempting to connect and execute a query
    with engine.connect() as connection:
        print("Database connection established successfully")
        
        
except SQLAlchemyError as e:
    print(f"Database connection error: {str(e)}")

# Define table schema
metadata = MetaData()

anonymized_dataset_table = Table(
    'anonymized_dataset', metadata,
    Column('unique_id', String, primary_key=True),
    Column('dataset_name', String),
    Column('size', String), 
    Column('issued', DateTime, default=func.now(), nullable=False),
    Column('data', JSONB)
)

sensitive_columns_table = Table(
    'sensitive_columns', metadata,
    Column('name', String, primary_key=True),
    Column('fields', JSONB),  # Store list of fields in one record
    Column('created_at', String),  # Store timestamp as string for consistency
    Column('domain_specific', ARRAY(String))  # Array of strings
)
# Create the table if it doesn't exist
metadata.create_all(engine)


data_product_dataset_table = Table(
    'data_product_dataset', metadata,
    autoload_with=engine
)

data_product_metadata_table = Table(
    'data_product_metadata', metadata,
    autoload_with=engine
)

@app.before_request
def reset_on_reload():
    global df, csv_loaded, unique_id_to_update, uploaded_file_format

    # Check if the current request is for Swagger UI
    if request.endpoint == 'swagger_ui.show':  # Replace this with your Swagger UI endpoint if different
        print("Reset triggered on Swagger UI reload")
        df = None
        csv_loaded = False
        unique_id_to_update = None
        uploaded_file_format = None

# This route ('/') is the default route that renders the 'index.html' template when someone accesses the root URL of the application.
@app.route('/')
def index():
    return render_template('index.html')
app.config['JSON_SORT_KEYS'] = False  # Optional: to ensure JSON keys are in the order they are added
# This route ('/load_data') is configured to handle POST requests. It expects a file to be sent in the request, 
# reads the CSV file using pandas, and filters the DataFrame to include only sensitive columns. 
# It then returns a JSON response indicating the status of the operation.
# Set the max content length to 100 MB
app.config['MAX_CONTENT_LENGTH'] = 1000 * 1024 * 1024  # 1000 MB
#COMMENT Linux command "sudo nano /etc/nginx/nginx.conf", "sudo nginx -s reload"

@app.errorhandler(RequestEntityTooLarge)
def handle_large_request(error):
    return jsonify({'status': 'error', 'message': 'File is too large. Please upload a smaller file.'}), 413

#                                                        FOR VM
#@app.route('/anonymization/swaggerfinal.json')
#def swagger_json():
#    return send_file('/home/datamiteedc/Desktop/nikos tools/anonymization tool/python_web_service/static/swaggerfinal.json')

def try_parse_json(field):
        try:
            return json.loads(field) if isinstance(field, str) else field
        except json.JSONDecodeError:
            return field
        

global unique_id_to_update
unique_id_to_update = None

def get_data_by_unique_ids_or_file_names(unique_ids=None, file_names=None):
    session = factory()
    try:
        j = join(data_product_metadata_table, data_product_dataset_table,
                 cast(data_product_metadata_table.c.unique_id, String) == cast(data_product_dataset_table.c.unique_id, String))
        
        query = select(data_product_metadata_table, 
                       *[column for column in data_product_dataset_table.c if column.name != 'unique_id']).select_from(j)
        
        conditions = []
        if unique_ids:
            conditions.append(data_product_metadata_table.c.unique_id.in_(unique_ids))
        if file_names:
            conditions.append(data_product_metadata_table.c.title.in_(file_names))

        query = query.where(or_(*conditions)) if conditions else query
        results = session.execute(query).mappings().all()
        # Track the unique IDs of the fetched results
        if results:
            
            modified_results = []

            for result in results:
                modified_result = dict(result)  # Create a mutable dictionary from the RowMapping
                modified_result['license'] = try_parse_json(modified_result['license'])
                modified_result['Spatial_coverage'] = try_parse_json(modified_result.get('Spatial_coverage', ''))
                modified_result['Temporal_coverage'] = try_parse_json(modified_result.get('Temporal_coverage', ''))
                modified_result['data_product_fields'] = try_parse_json(modified_result.get('data_product_fields', ''))
                modified_result['distribution'] = try_parse_json(modified_result.get('distribution', ''))
                modified_result['termsAndConditions'] = try_parse_json(modified_result.get('termsAndConditions', ''))
    
                modified_results.append(modified_result)
            return modified_results    # Convert each result to a dictionary
        else:
            return None

    except SQLAlchemyError as e:
        print(f"Database error: {str(e)}")
        return None
    finally:
        session.close()

@app.route('/anonymization/get_data_product', methods=['GET'])
def get_data_product():
    global df, csv_loaded, unique_id_to_update

    # Reset global variables before processing
    #df = None
    #csv_loaded = False

    unique_ids = request.args.get('unique_id')
    file_names = request.args.get('title')

    unique_ids = unique_ids.split(',') if unique_ids else []
    file_names = file_names.split(',') if file_names else []

    results = get_data_by_unique_ids_or_file_names(unique_ids=unique_ids, file_names=file_names)

    if results:
        # Ensure 'data' field is properly structured before converting to DataFrame
        data_content = []
        metadata_content = []

        for result in results:
            # Append the metadata fields
            metadata_content.append({key: result[key] for key in result.keys() if key not in ['data']})

            data = result.get('data')
            if isinstance(data, str):  # If `data` is a JSON string
                data = json.loads(data)
            if isinstance(data, dict):  # If `data` is in dictionary format
                data_content.append(data)
            elif isinstance(data, list):  # If `data` is a list of records
                data_content.extend(data)
            else:
                return jsonify({'error': 'Unexpected data format in result'}), 500
            
            unique_id_to_update = result.get('unique_id')

        # Convert to DataFrame and verify correct column names
        df = pd.DataFrame(data_content)

        # Check the structure of `df` for debugging purposes
        print("Columns in df:", df.columns)

        # Set csv_loaded to True to indicate that data is loaded
        csv_loaded = True

        # Prepare the response including both metadata and data
        return jsonify({
            'status': 'success',
            #'data': df.to_dict(orient="records"),
            'metadata': metadata_content
        }), 200
    else:
        return jsonify({'error': 'No data found for provided IDs or titles'}), 404
    
@app.route('/anonymization/get_saved_data_fromanonymization', methods=['GET'])
def get_saved_data_fromanonymization():
    """
    This endpoint retrieves up to 10 unique IDs and their corresponding data
    from the anonymized_dataset table in the database.
    """
    session = factory()
    try:
        # Query the anonymized_dataset table to get 10 rows
        query = select(
            anonymized_dataset_table.c.unique_id,
            anonymized_dataset_table.c.data
        ).limit(10)
        results = session.execute(query).mappings().all()

        # Prepare the response
        anonymized_data_list = [
            {"unique_id": result["unique_id"], "data": result["data"]}
            for result in results
        ]

        return jsonify({"status": "success", "anonymized_data": anonymized_data_list}), 200
    except SQLAlchemyError as e:
        print(f"Database error: {str(e)}")
        return jsonify({"status": "error", "message": "Database error occurred"}), 500
    finally:
        session.close()


@app.route('/anonymization/load_data', methods=['POST'])
def load_data():
    global df,csv_loaded, uploaded_file_format
    try:
        file = request.files['file']

        if file.filename == '':
            return jsonify({'status': 'error', 'message': 'No selected file'}), 400
        
        # Get the file extension
        filename = secure_filename(file.filename)
        file_extension = filename.rsplit('.', 1)[-1].lower()

        # Check if the file is a CSV or XLSX file
        if file_extension not in ['csv', 'xlsx', 'json']:
            return jsonify({'status': 'error', 'message': 'Invalid file format'}), 400 
        

        # Assuming the file is sent as form data in a POST request
        if file_extension == 'csv':
            df = pd.read_csv(file)
        elif file_extension == 'xlsx':
            df = pd.read_excel(file)
        elif file_extension == 'json':
            df = pd.read_json(file)

        # Normalize column names
        #df.columns = [col.split('.')[0] for col in df.columns]


        # Check for exact duplicate columns

        exact_duplicates = df.columns[df.columns.duplicated(keep=False)].tolist()

        if exact_duplicates:
            duplicate_columns = list(set(exact_duplicates))  # Get unique column names
            return jsonify({'status': 'error', 'message': f'The uploaded file contains duplicate columns: {duplicate_columns}'}), 400
        lower_case_columns = df.columns.str.lower()        
        # Check for case-insensitive duplicate columns
        if lower_case_columns.duplicated().any():
            return jsonify({'status': 'error', 'message': 'The uploaded file contains case-insensitive duplicate columns.'}), 400




        # Check if the user is uploading the same file again
        #if csv_loaded and uploaded_file_format == file_extension and df.equals(pd.read_csv(file) if file_extension == 'csv' else pd.read_excel(file) if file_extension == 'xlsx' else pd.read_json(file)):
        #    return jsonify({'status': 'error', 'message': 'The same file has already been uploaded.'}), 400      

        # Check for mixed data types in each column
        mixed_type_columns = []
        for column in df.columns:
            column_type = df[column].map(type).nunique()
            if column_type > 1:
                mixed_type_columns.append(column)
        
        if mixed_type_columns:
            return jsonify({
                'status': 'error',
                'message': f'The following columns contain mixed data types: {mixed_type_columns}. Please ensure each column contains only one data type.'
            }), 400            
            
        csv_loaded = True
        uploaded_file_format = file_extension
        return jsonify({'status': 'success', 'message': 'Data loaded successfully'})
    except pd.errors.EmptyDataError:
        return jsonify({'status': 'error', 'message': 'Error: The file is empty.'})
    except pd.errors.ParserError:
        return jsonify({'status': 'error', 'message': 'Error: Unable to parse the file. Please check the file format, delimiter, or encoding.'})
    except Exception as e:
        return jsonify({'status': 'error', 'message': f'Error: {str(e)}'})
    
@app.route('/anonymization/get_predifined_sensitive_columns_by_datamite', methods=['GET'])
def get_predifined_sensitive_columns_by_datamite():
    try:
        # Return the lists of sensitive columns for both languages
        return jsonify({
            'status': 'success',
            'message': 'Supported sensitive columns retrieved successfully.',
            'data': {
                'general_sensitive_columns': general_sensitive_columns,
                'general_sensitive_columns_gr': general_sensitive_columns_gr
            }
        })
    except Exception as e:
        return jsonify({'status': 'error', 'message': f'Error retrieving supported sensitive columns: {str(e)}'}), 500
    
@app.route('/anonymization/get_all_metadata', methods=['GET'])
def get_all_metadata():
    session = None
    try:
        # Create a session to interact with the database
        session = factory()

        # Get the number of records from the query parameter (with a default of 20, and a maximum of 100)
        num_records = request.args.get('limit', default=20, type=int)
        num_records = min(num_records, 100)  # Limiting the max number of records to 100

        # Fetch metadata records ordered by 'issued' date
        results = session.query(anonymized_dataset_table).order_by(anonymized_dataset_table.c.issued.desc()).limit(num_records).all()

        # Transform the results into a list of dictionaries for JSON serialization
        metadata_list = []
        for result in results:       
            metadata_dict = {
                'unique_id': result.unique_id,
                'dataset_name': result.dataset_name,
                'issued': result.issued,
                'size': result.size
            }
            metadata_list.append(metadata_dict)

        # Return the metadata as JSON
        return jsonify({'status': 'success', 'data': metadata_list}), 200

    except Exception as e:
        return jsonify({'status': 'error', 'message': str(e)}), 500

    finally:
        if session:
            session.close()   


    
@app.route('/anonymization/add_custom_list_sensitive_columns', methods=['POST'])
def add_custom_list_sensitive_columns():
    data = request.get_json()
    name = data.get('name')  # Custom name provided by the user
    fields = data.get('fields')  # List of sensitive fields from the user
    domain_specific = data.get('domain_specific')  # Optional domain-specific array

    # Validate required fields
    if not name or not fields:
        return jsonify({'status': 'error', 'message': 'Both "name" and "fields" are required.'}), 400

    # Ensure domain_specific is a list (if provided)
    if domain_specific and isinstance(domain_specific, list):
        domain_specific = [str(item) for item in domain_specific]  # Ensure all items are strings
    else:
        domain_specific = []  # If not provided, use an empty list

    try:
        # Start a new session with transaction handling
        with engine.connect() as connection:
            # Insert the data into the table as a single record
            ins = sensitive_columns_table.insert().values(
                name=name,
                fields=fields,  # Insert fields as a list (not JSON)
                domain_specific=domain_specific,  # Insert domain_specific as a list of strings
                created_at=datetime.now().isoformat()
            )
            result = connection.execute(ins)
            connection.commit()  # Explicitly commit the transaction

            # Check if insertion was successful
            if result.rowcount > 0:
                return jsonify({
                    'status': 'success',
                    'message': f'Sensitive fields successfully added to the database and the name of the list is {name}.'
                })
            else:
                return jsonify({
                    'status': 'error',
                    'message': 'Failed to insert sensitive fields in the database.'
                }), 500

    except SQLAlchemyError as e:
        return jsonify({'status': 'error', 'message': f'Database error: {str(e)}'}), 500

@app.route('/anonymization/update_custom_list_sensitive_columns', methods=['PUT'])
def update_custom_list_sensitive_columns():
    data = request.get_json()
    name = data.get('name')  # Name of the list to update
    new_fields = data.get('fields')  # New list of sensitive fields
    domain_specific = data.get('domain_specific')  # New domain-specific fields

    # Validate required fields
    if not name or not new_fields:
        return jsonify({'status': 'error', 'message': 'Both "name" and "fields" are required.'}), 400

    try:
        # Start a new session with transaction handling
        with engine.connect() as connection:
            # Delete the previous record for this name
            del_stmt = sensitive_columns_table.delete().where(sensitive_columns_table.c.name == name)
            connection.execute(del_stmt)

            # Insert the new fields as a single record
            ins_stmt = sensitive_columns_table.insert().values(
                name=name,
                fields=new_fields,  # Directly insert as JSONB, no json.dumps()
                domain_specific=domain_specific,  # Directly insert as JSONB, no json.dumps()
                created_at=datetime.now().isoformat()
            )
            connection.execute(ins_stmt)
            connection.commit()  # Explicitly commit the transaction

            return jsonify({
                'status': 'success',
                'message': f'Sensitive fields for {name} have been updated successfully.'
            })

    except SQLAlchemyError as e:
        return jsonify({'status': 'error', 'message': f'Database error: {str(e)}'}), 500
    

@app.route('/anonymization/delete_custom_lists', methods=['DELETE'])
def delete_custom_lists():
    data = request.get_json()
    names = data.get('names')  # List of names to delete

    # Validate required field
    if not names or not isinstance(names, list):
        return jsonify({'status': 'error', 'message': 'The "names" field is required and should be a list.'}), 400

    try:
        # Start a new session with transaction handling
        with engine.connect() as connection:
            # Delete the records for the provided names
            delete_stmt = sensitive_columns_table.delete().where(sensitive_columns_table.c.name.in_(names))
            result = connection.execute(delete_stmt)
            connection.commit()  # Explicitly commit the transaction

            # Check if deletion was successful
            if result.rowcount > 0:
                return jsonify({
                    'status': 'success',
                    'message': f'{result.rowcount} custom lists have been deleted successfully.'
                })
            else:
                return jsonify({
                    'status': 'error',
                    'message': 'No custom lists were found to delete.'
                }), 404

    except SQLAlchemyError as e:
        return jsonify({'status': 'error', 'message': f'Database error: {str(e)}'}), 500    
    
@app.route('/anonymization/get_all_custom_lists', methods=['GET'])
def get_all_custom_lists():
    try:
        # Start a new session to query the database
        with engine.connect() as connection:
            # Query to select all columns from sensitive_columns_table
            select_stmt = sensitive_columns_table.select()
            result = connection.execute(select_stmt)
            rows = result.fetchall()  # Fetch all rows from the result set

            # Convert rows to a list of dictionaries by extracting column names and values
            custom_lists = []
            for row in rows:
                custom_list_dict = dict(row._mapping)

                # Convert 'fields' from string format to JSON array if it's a string
                if isinstance(custom_list_dict['fields'], str):
                    try:
                        # Parse the stringified JSON to a Python list
                        custom_list_dict['fields'] = json.loads(custom_list_dict['fields'])
                    except json.JSONDecodeError:
                        # If the fields cannot be decoded, keep it as is
                        custom_list_dict['fields'] = []

                if isinstance(custom_list_dict.get('domain_specific'), str):
                    try:
                        custom_list_dict['domain_specific'] = json.loads(custom_list_dict['domain_specific'])
                    except json.JSONDecodeError:
                        custom_list_dict['domain_specific'] = []        

                custom_lists.append(custom_list_dict)

            # Return the results as a JSON response
            return jsonify({
                'status': 'success',
                'message': 'Custom lists retrieved successfully.',
                'data': custom_lists
            })

    except SQLAlchemyError as e:
        return jsonify({'status': 'error', 'message': f'Database error: {str(e)}'}), 500
    
@app.route('/anonymization/get_custom_lists', methods=['GET'])
def get_custom_lists():
    try:
        # Get the list of names from the query parameters
        list_names = request.args.getlist('name')  # Supports multiple names by passing 'name' multiple times

        # If list_names contains only one item and has commas, split it into multiple names
        if len(list_names) == 1 and ',' in list_names[0]:
            list_names = [name.strip() for name in list_names[0].split(',')]        

        with engine.connect() as connection:
            # If specific names are provided, filter based on those; otherwise, fetch all
            if list_names:
                # Query to select rows where the name is in the provided list of names
                select_stmt = sensitive_columns_table.select().where(
                    sensitive_columns_table.c.name.in_(list_names)
                )
            else:
                # Select all rows if no names are provided
                select_stmt = sensitive_columns_table.select()
                
            result = connection.execute(select_stmt)
            rows = result.fetchall()

            # Convert rows to a list of dictionaries by extracting column names and values
            custom_lists = []
            for row in rows:
                custom_list_dict = dict(row._mapping)

                # Convert 'fields' from string format to JSON array if it's a string
                if isinstance(custom_list_dict['fields'], str):
                    try:
                        custom_list_dict['fields'] = json.loads(custom_list_dict['fields'])
                    except json.JSONDecodeError:
                        custom_list_dict['fields'] = []

                if isinstance(custom_list_dict.get('domain_specific'), str):
                    try:
                        custom_list_dict['domain_specific'] = json.loads(custom_list_dict['domain_specific'])
                    except json.JSONDecodeError:
                        custom_list_dict['domain_specific'] = []         

                custom_lists.append(custom_list_dict)

            # Return the results as a JSON response
            return jsonify({
                'status': 'success',
                'message': 'Custom lists retrieved successfully.',
                'data': custom_lists
            })

    except SQLAlchemyError as e:
        return jsonify({'status': 'error', 'message': f'Database error: {str(e)}'}), 500

    
@app.route('/anonymization/user_custom_list', methods=['POST'])
def user_custom_list():
    global df, csv_loaded
    if not csv_loaded:
        return jsonify({'status': 'error', 'message': 'Error: No data loaded. Please load a CSV file first.'}), 400

    try:
        sensitive_columns = request.json.get('sensitive_columns', [])
        if not sensitive_columns:
            return jsonify({'status': 'error', 'message': 'No sensitive columns provided'}), 400

        identified_columns = {}

        for target_column_name in sensitive_columns:
            for col_name in df.columns:
                # Normalize and preprocess column names
                col_name_no_tones = ''.join(c for c in unicodedata.normalize('NFD', col_name) if unicodedata.category(c) != 'Mn')
                col_name_preprocessed = col_name_no_tones.lower().replace("_", " ").replace(".", "")
                
                # Tokenize and lemmatize column names
                col_tokens = word_tokenize(col_name_preprocessed)
                col_tokens = [lemmatizer.lemmatize(token) for token in col_tokens]

                # Tokenize and lemmatize target column names
                target_tokens = word_tokenize(target_column_name.lower())
                target_tokens = [lemmatizer.lemmatize(token) for token in target_tokens]

                # Calculate similarity
                intersection = set(col_tokens) & set(target_tokens)
                similarity_score = len(intersection) / len(target_tokens)  # Jaccard similarity
                
                if similarity_score > 0.4:  # Adjust threshold as needed
                    identified_columns[target_column_name] = {
                        "column_name": col_name,
                        "similarity_score": similarity_score,
                    }
                    break

        return jsonify({'status': 'success', 'identified_columns': identified_columns})

    except Exception as e:
        return jsonify({'status': 'error', 'message': f'Error: {str(e)}'})


#@app.route('/anonymize', methods=['GET'])
#def show_anonymization_options():
 #   anonymization_handler.show_anonymization_options()

"""
@app.route('/post_user_info', methods=['POST'])
def post_user_info():
    try:
        username = request.form['username']
        email = request.form['email']

        # Do something with the user information (e.g., store it in a database)
        # You can replace the print statements with your desired logic

        print(f"Received POST request with username: {username} and email: {email}")

        return jsonify({'status': 'success', 'message': 'User information received successfully'})
    except Exception as e:
        return jsonify({'status': 'error', 'message': f'Error in post_user_info: {str(e)}'})
"""

# This route ('/get_sensitive_columns') is configured to handle GET requests. 
# It iterates through each sensitive column specified in sensitive_columns. 
# For each column, it checks if the column exists in the loaded DataFrame (df). 
# If the column exists, it retrieves the first 6 rows of data, along with metadata such as data type, unique values, and missing values. 
# This information is then organized into a dictionary called sensitive_data.
class NpEncoder(json.JSONEncoder):
    def default(self, obj):
        dtypes = (np.datetime64, np.complexfloating)
        if isinstance(obj, dtypes):
            return str(obj)
        elif isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            if any([np.issubdtype(obj.dtype, i) for i in dtypes]):
                return obj.astype(str).tolist()
            return obj.tolist()
        return super(NpEncoder, self).default(obj)

@app.route('/anonymization/identify_columns', methods=['POST'])
def identify_columns():
    global df, csv_loaded, general_sensitive_columns, general_sensitive_columns_gr, sensitive_columns
    try:

        if df is None or not csv_loaded:
            #print("First few rows of DataFrame:\n", df.head())
            print("CSV Loaded:", csv_loaded)
            return jsonify({'status': 'error', 'message': 'Error: No data loaded. Please load a file first.'})


        
        # Retrieve optional domain_specific and custom_list_name from the request body
        data = request.get_json()
        domain_specific = data.get('domain_specific', [])
        custom_list_name = data.get('name', [])

        # Dictionary to hold identified columns
        identified_columns = {}

        # List to store all match details
        all_matches = []

        # 1. General Sensitive Column Identification
        for col_name in df.columns:
            for target_column_name in general_sensitive_columns + general_sensitive_columns_gr:
                col_name_no_tones = ''.join(c for c in unicodedata.normalize('NFD', col_name) if unicodedata.category(c) != 'Mn')
                col_name_preprocessed = col_name_no_tones.lower().replace("_", " ").replace(".", "")
                
                character_match_score = SequenceMatcher(None, col_name_preprocessed, target_column_name.lower()).ratio()

                if character_match_score >= 0.65:
                    match_info = {
                        "column_name": col_name,
                        "character_match_score": character_match_score,
                        "source": "general",
                        "col_name": target_column_name
                    }
                    all_matches.append(match_info)

        custom_fields = set()
        
        with engine.connect() as connection:
            if custom_list_name:
                select_stmt = sensitive_columns_table.select().where(
                    sensitive_columns_table.c.name.in_(custom_list_name)
                )
                results = connection.execute(select_stmt).fetchall()
                for result in results:
                    fields_data = result[1]
                    if isinstance(fields_data, str):
                        custom_fields.update(json.loads(fields_data))
                    elif isinstance(fields_data, list):
                        custom_fields.update(fields_data)
            
            if domain_specific:
                select_stmt = sensitive_columns_table.select().where(
                    sensitive_columns_table.c.domain_specific.op('&&')(literal(domain_specific))
                )
                results = connection.execute(select_stmt).fetchall()
                for result in results:
                    fields_list = result[1]
                    if isinstance(fields_list, list):
                        custom_fields.update(fields_list)

            # Perform character_match_score comparisons for custom fields
            if custom_fields:
                for col_name in df.columns:
                    for target_column_name in custom_fields:
                        col_name_no_tones = ''.join(c for c in unicodedata.normalize('NFD', col_name) if unicodedata.category(c) != 'Mn')
                        col_name_preprocessed = col_name_no_tones.lower().replace("_", " ").replace(".", "")

                        character_match_score = SequenceMatcher(None, col_name_preprocessed, target_column_name.lower()).ratio()

                        if character_match_score >= 0.65:
                            match_info = {
                                "column_name": col_name,
                                "character_match_score": character_match_score,
                                "source": "custom",
                                "col_name": target_column_name
                            }
                            all_matches.append(match_info)

        # First filter: Keep the best match for each (column_name, source) combination across all columns
        best_matches = {}
        for match in all_matches:
            key = (match["column_name"], match["source"])
            # If there is no match for this key or the current match has a better score, update it
            if key not in best_matches or best_matches[key]["character_match_score"] < match["character_match_score"]:
                best_matches[key] = match

        # Second filter: Ensure only one target_column_name per (col_name, source) within each column
        final_matches = {}
        for match in best_matches.values():
            key = (match["col_name"], match["source"])
            # If there is no match for this col_name/source pair, or this match has a better score, replace it
            if key not in final_matches or final_matches[key]["character_match_score"] < match["character_match_score"]:
                final_matches[key] = match

        # Populate identified_columns based on the final_matches dictionary
        for match in final_matches.values():
            identified_columns.setdefault(match["col_name"], []).append({
                "column_name": match["column_name"],
                "character_match_score": match["character_match_score"],
                "source": match["source"]
            })

        app.identified_columns = identified_columns


        
        return jsonify({
            'status': 'success', 
            'identified_columns': app.identified_columns
        })

    except Exception as e:
        return jsonify({'status': 'error', 'message': f'Error: {str(e)}'})


    
@app.route('/anonymization/get_sensitive_columns', methods=['GET'])
def get_sensitive_columns():
    global csv_loaded
    try:
        if not csv_loaded:
            return jsonify({'status': 'error', 'message': 'Error: No data loaded. Please load a CSV file first.'})
        
        identified_columns = app.identified_columns
        sensitive_data = {}

        for col_name, identified_column_list in identified_columns.items():
            for identified_column in identified_column_list:  # Loop through the list of matches
                column_name = identified_column["column_name"]
                if column_name in df.columns:
                    column_data = df[column_name].head(3).tolist()
                    sensitive_data[column_name] = {
                        'data': column_data,
                        'dtype': str(df[column_name].dtype),
                        'unique_values': df[column_name].nunique(),
                        'missing_values': df[column_name].isnull().sum()
                    }

        return json.dumps({'status': 'success', 'sensitive_data': sensitive_data}, cls=NpEncoder, ensure_ascii=False)
    except Exception as e:
        return jsonify({'status': 'error', 'message': f'Error in get_sensitive_columns: {str(e)}'})

@app.route('/anonymization/post_column_info', methods=['POST'])
def post_column_info_route():
    try:
        json_data = request.get_json()

        if 'selected_column' not in json_data:
            raise ValueError('The key "selected_column" is missing in the JSON data.')

        identified_columns = app.identified_columns
        selected_column = json_data['selected_column']
        column_info = {}

        custom_fields = []  # Store custom fields selected by the user
        custom_fields_generalisation = []

        for selected_column1 in selected_column:
            techniques = []
            for target_column_name, info_list in identified_columns.items():
                for info in info_list:
                    character_match_score = info["character_match_score"]
                    if character_match_score >= 0.65 and info["column_name"].lower() == selected_column1.lower():
                        # Determine source type
                        source = "custom" if info["source"] == "custom" else "general"

                        # Store in custom fields list if it's a custom column
                        if source == "custom":
                            print(f"Appending to custom_fields: {selected_column1}")
                            custom_fields.append(target_column_name)
                            custom_fields_generalisation.append(selected_column1)
                            update_user_selected_custom_fields(df, custom_fields, custom_fields_generalisation)


                        # Call post_column_info with source
                        result = post_column_info(target_column_name, source)
                        print(f"Result from post_column_info: {result}")

                        if result['status'] == 'error':
                            raise ValueError(f"Error in post_column_info: {result['message']}")
                        
                        techniques.extend(result['allowed_techniques'])
            column_info[selected_column1] = list(set(techniques))  # Avoid duplicates
        # ✅ Send only if there are custom fields

            

        return jsonify({'status': 'success', 'column_info': column_info})
    except Exception as e:
        return jsonify({'status': 'error', 'message': f'Error in post_column_info_route: {str(e)}'})

    

    
@app.route('/anonymization/post_anonymization_info', methods=['POST'])
def post_anonymization_info_route():
    global df, temp_storage, anonymized_dataset_table, engine, anonymized_data

    try:
        # Check if df is initialized and not None
        if df is None or df.empty:
            raise ValueError("The DataFrame 'df' is not initialized or is empty. Please load data first.")
        
        json_data = request.get_json()

        if 'selected_column' not in json_data or 'selected_techniques' not in json_data:
            raise ValueError('The keys "selected_column" and "selected_techniques" are missing in the JSON data.')

        selected_columns = json_data['selected_column']
        selected_techniques = json_data['selected_techniques']

        if len(selected_columns) != len(selected_techniques):
            raise ValueError('The length of "selected_column" and "selected_techniques" must be the same.')

        # Create a copy of df to work with
        working_df = df.copy()

        logging.info("Starting the anonymization process...")

        for column, technique in zip(selected_columns, selected_techniques):
            logging.info(f"Processing column '{column}' with technique '{technique}'")
            logging.info(f"Original working_df before anonymization for column '{column}':")
            logging.info(working_df.head())
            logging.info(f"DataFrame before anonymization:\n{working_df.head()}")

            for target_column_name, info_list in app.identified_columns.items():
                for info in info_list:  # Iterate over the list of info dictionaries
                    character_match_score = info["character_match_score"]
                    if character_match_score >= 0.65 and info["column_name"].lower() == column.lower():
                        logging.info(f"Anonymizing column '{column}' using technique '{technique}'")

                        results = post_anonymization_info(working_df, target_column_name, technique, column)

                        if results['status'] == 'error':
                            raise ValueError(results['message'])

                        anonymized_df = pd.read_json(results['anonymized_value'], orient='records')

                        if anonymized_df is None or anonymized_df.empty:
                            raise ValueError('The DataFrame created from the JSON data is None or empty.')

                        # Update the working DataFrame with the anonymized values
                        working_df.update(anonymized_df)

                        logging.info(f"Updated working_df after anonymization for column '{column}':")
                        logging.info(working_df.head())
                        logging.info(f"DataFrame after anonymization:\n{working_df.head()}")

                        break

        # Ensure anonymized data exists
        if working_df.empty:
            raise ValueError('No valid anonymized data was generated.')

        # Replace NaN values with None before converting to JSON
        working_df = working_df.where(pd.notnull(working_df), None)

        # Convert the anonymized DataFrame to JSON
        anonymized_json = working_df.to_dict(orient='records')

        # Limit the output to the first 10 rows
        anonymized_json_preview = anonymized_json[:10]

        # Log final anonymized data preview
        logging.info("Final anonymized JSON data:")
        logging.info(anonymized_json[:5])
        anonymized_data = anonymized_json

        # Return the anonymized data
        return jsonify({
            'status': 'success',
            'anonymized_data_preview': anonymized_json_preview
        })

    except Exception as e:
        logging.error(f"Error in anonymize_data_route: {str(e)}")
        return jsonify({'status': 'error', 'message': f'Error in anonymize_data_route: {str(e)}'})


@app.route('/anonymization/save_anonymized_data_into_anonymization_table', methods=['POST'])
def save_anonymized_data_into_anonymization_table():
    global anonymized_dataset_table, engine, anonymized_data 
    try:
        data = request.json 
        dataset_name = data.get('dataset_name')

        # Ensure dataset_name is provided
        if not dataset_name:
            raise ValueError('Dataset name is required.')
        
        # Use the anonymized data directly from your global context
        if 'anonymized_data' not in globals() or anonymized_data is None:
            raise ValueError('No anonymized data available to save.')

        # Convert anonymized_data to a DataFrame for processing
        anonymized_df = pd.DataFrame(anonymized_data)

        # Replace NaN values with None
        anonymized_df = anonymized_df.applymap(lambda x: None if pd.isna(x) else x)

        # Convert the DataFrame back to a JSON-compatible dictionary
        anonymized_json = anonymized_df.to_dict(orient='records')

        # Ensure that any NaN values are converted to None in the final JSON data
        for record in anonymized_json:
            for key, value in record.items():
                if isinstance(value, float) and pd.isna(value):
                    record[key] = None

        # ✅ Compute size using sys.getsizeof (convert to KB)
        dataset_size_kb = sys.getsizeof(anonymized_json) / 1024  # Convert bytes to KB
        dataset_size_kb = round(dataset_size_kb, 2)  # Round to 2 decimal places

        # ✅ Get current timestamp for "issued"
        issued_timestamp = datetime.utcnow()

        # Generate a unique ID for the anonymized data
        unique_id = str(uuid.uuid4())

                # 🔹 Check if a dataset with the same dataset_name already exists
        existing_dataset = session.query(anonymized_dataset_table).filter_by(dataset_name=dataset_name).first()
        if existing_dataset:
            logging.warning(f"A dataset with the name '{dataset_name}' already exists.")
            return jsonify({
                'status': 'error',
                'message': f"A dataset with the name '{dataset_name}' already exists. Please use a different name."
            })

        # Check if a dataset with the same values already exists
        existing_record = session.query(anonymized_dataset_table).filter_by(data=anonymized_json).first()

        if existing_record:
            logging.warning("Dataset with the same values already exists.")
            return jsonify({
                'status': 'error',
                'message': 'A dataset with the same values already exists in the database.'
            })

        logging.info(f"Inserting anonymized data with unique_id: {unique_id}")
        new_rec = anonymized_dataset_table.insert().values(
            unique_id=unique_id, 
            dataset_name=dataset_name,
            size=f"{dataset_size_kb} KB",  # Save size in KB
            issued=issued_timestamp,  # Save issued timestamp
            data=anonymized_json
            )
        session.execute(new_rec)
        session.commit()  # Commit after successful execution
        logging.info(f"Data saved to PostgreSQL with unique_id: {unique_id}")

        # Generate the URL for the anonymized data
        json_url = url_for('get_anonymized_data', token=unique_id, _external=True)
        return jsonify({
            'status': 'success',
            'message': 'Anonymized dataset saved to PostgreSQL successfully',
            'json_url': json_url
        })

    except Exception as e:
        logging.error(f"Error in save_anonymized_data_into_anonymization_table: {str(e)}")
        session.rollback()  # Rollback the transaction on error
        return jsonify({'status': 'error', 'message': f'Error in save_anonymized_data_into_anonymization_table: {str(e)}'})
    

@app.route('/anonymization/delete_anonymized_datasets', methods=['DELETE'])
def delete_anonymized_datasets():
    try:
        # Get the list of unique_ids from the request
        data = request.json
        unique_ids_str = data.get('unique_ids')

        # Check if unique_ids_str is missing
        if not unique_ids_str:
            return jsonify({'status': 'error', 'message': 'unique_ids are required'}), 400
        
        # Split the comma-separated IDs and strip any whitespace
        unique_ids = [uid.strip() for uid in unique_ids_str.split(',') if uid.strip()]

        # Pass the unique_ids to the logic function for deletion
        status, message = delete_anonymized_datasets_logic(unique_ids)
        
        # Return appropriate response
        if status == 'success':
            return jsonify({'status': 'success', 'message': message}), 200
        else:
            return jsonify({'status': 'error', 'message': message}), 500

    except Exception as e:
        return jsonify({'status': 'error', 'message': str(e)}), 500


def delete_anonymized_datasets_logic(unique_ids):
    global anonymized_dataset_table, session
    try:
        # Begin transaction
        with session.begin():
            # Fetch the records that exist in the dataset table with the provided unique_ids
            existing_ids = session.query(anonymized_dataset_table.c.unique_id).filter(
                anonymized_dataset_table.c.unique_id.in_(unique_ids)
            ).all()
            
            # Convert the fetched tuples to a list of unique IDs
            existing_ids = [eid[0] for eid in existing_ids]

            # Find the IDs that are missing (not in the database)
            missing_ids = set(unique_ids) - set(existing_ids)
            found_ids = set(unique_ids) & set(existing_ids)

            if found_ids:
                # Proceed to delete records from both tables if there are valid IDs to delete
                session.query(anonymized_dataset_table).filter(
                    anonymized_dataset_table.c.unique_id.in_(found_ids)
                ).delete(synchronize_session=False)



            # Commit the transaction
            session.commit()

            # Prepare the message based on found and missing IDs
            success_message = f"Successfully deleted anonymized datasets with IDs: {', '.join(found_ids)}" if found_ids else ""
            missing_message = f"The following unique_ids do not exist in the database: {', '.join(missing_ids)}" if missing_ids else ""

            # Return both success and missing messages
            return 'success', f"{success_message}. {missing_message}".strip()

    except SQLAlchemyError as e:
        session.rollback()  # Rollback in case of error
        return 'error', f"Database error: {str(e)}"
    
    except Exception as e:
        return 'error', f"Unexpected error: {str(e)}"    
    
@app.route('/anonymization/anonymized_data/<token>', methods=['GET'])
def get_anonymized_data(token):
    global anonymized_dataset_table
    try:
        # Retrieve the data from the database
        #query = anonymized_dataset_table.select().where(anonymized_dataset_table.c.unique_id == token)

        result=session.query(anonymized_dataset_table).filter_by(unique_id=token).first()
        logging.info(f"Result: {result.unique_id}")
        #with engine.connect() as connection:
        #   result = connection.execute(query).fetchone()

        if result:
            json_data = result.data
            return jsonify({'status': 'success', 'data': json_data})
        else:
            return jsonify({'status': 'error', 'message': 'Invalid token or data not found'}), 404
    except SQLAlchemyError as e:
        logging.error(f"Database error: {str(e)}")
        return jsonify({'status': 'error', 'message': f'Database error: {str(e)}'}), 500
    except Exception as e:
        logging.error(f"Error in get_anonymized_data: {str(e)}")
        return jsonify({'status': 'error', 'message': f'Error in get_anonymized_data: {str(e)}'}), 500

def try_parse_json(field):
        try:
            return json.loads(field) if isinstance(field, str) else field
        except json.JSONDecodeError:
            return field  # Return as is if it's not valid JSON  
        
@app.route('/anonymization/update_data_product_with_anonymized_data', methods=['POST'])
def update_data_product_with_anonymized_data():
    global anonymized_data, session, engine, data_product_metadata_table, data_product_dataset_table, unique_id_to_update

    try:
        # Ensure anonymized data is available
        if 'anonymized_data' not in globals() or anonymized_data is None:
            return jsonify({'status': 'error', 'message': 'No anonymized data found to update.'}), 400

        # Check for a valid unique ID
        if not unique_id_to_update:
            return jsonify({'status': 'error', 'message': 'No valid unique ID found to update records.'}), 400

        # Ensure anonymized_data is in the correct format (list, dict, or valid JSON string)
        if isinstance(anonymized_data, (list, dict)):
            anonymized_json = anonymized_data  # Keep as raw data
        elif isinstance(anonymized_data, str):
            try:
                # If it's a string, ensure it's valid JSON
                anonymized_json = json.loads(anonymized_data)  # Convert string to JSON object
            except json.JSONDecodeError:
                return jsonify({'status': 'error', 'message': 'Invalid JSON format in anonymized_data'}), 400
        else:
            return jsonify({'status': 'error', 'message': 'Anonymized data must be a list, dict, or valid JSON string.'}), 400

        # Retrieve the existing metadata record
        existing_metadata = session.query(data_product_metadata_table).filter_by(unique_id=unique_id_to_update).first()
        if not existing_metadata:
            return jsonify({'status': 'error', 'message': 'Data product not found in the database'}), 404

        # Update the distribution field
        existing_distribution = existing_metadata.distribution or []
        if isinstance(existing_distribution, str):
            existing_distribution = json.loads(existing_distribution)  # Ensure it's parsed into a list of dictionaries

        for dist in existing_distribution:
            if 'format' in dist and dist['format'] == 'application/json':
                dist['accessURL'] = dist.get('accessURL', '')  # Keep existing URL

        # Increment the version
        current_version = existing_metadata.version or '1'
        new_version = str(int(current_version) + 1)

        # Anonymization date for support_tools_actions
        anonymization_date = datetime.now()
        anonymization_date_str = f"anonymized: {anonymization_date}"

        # Calculate the size of the updated dataset
        size = sys.getsizeof(json.dumps(anonymized_json))

        # Ensure anonymized_data is serialized correctly without double encoding
        if isinstance(anonymized_json, (list, dict)):
            # If it's a list or dict, it's already in the correct format, so directly use it
            data_product_fields = anonymized_json
        elif isinstance(anonymized_json, str):
            try:
                # If it's a string, ensure it's valid JSON
                anonymized_json = json.loads(anonymized_json)  # Convert string to JSON object
                data_product_fields = anonymized_json
            except json.JSONDecodeError:
                return jsonify({'status': 'error', 'message': 'Invalid JSON format in anonymized_data'}), 400
        else:
            return jsonify({'status': 'error', 'message': 'Anonymized data must be a list, dict, or valid JSON string.'}), 400

        # Update the modified field
        current_modifications = existing_metadata.modified or []  # Get existing list of timestamps or empty list
        if not isinstance(current_modifications, list):
            current_modifications = [current_modifications]  # Ensure it's a list

        # Append the current timestamp
        current_time = datetime.now()
        updated_modifications = current_modifications + [current_time]
        
        # Prepare the metadata update query
        metadata_update_query = (
            data_product_metadata_table.update()
            .where(data_product_metadata_table.c.unique_id == unique_id_to_update)
            .values(
                version=new_version,
                distribution=existing_distribution,  # Ensure it's stored as JSON, not double-encoded
                support_tools_actions=anonymization_date_str,
                size=size,
                modified=updated_modifications,
                data_product_fields=data_product_fields  

            )
        )

        # Prepare the dataset update query
        dataset_update_query = (
            data_product_dataset_table.update()
            .where(data_product_dataset_table.c.unique_id == unique_id_to_update)
            .values(data=anonymized_json)  # Directly store JSON data
        )

        # Execute the updates in a transaction
        with engine.begin() as connection:
            connection.execute(metadata_update_query)
            connection.execute(dataset_update_query)

        return jsonify({
            'status': 'success',
            'message': 'Data product successfully updated with anonymized data and metadata.',
            #'data': anonymized_json  # Return the data without extra serialization
        }), 200

    except SQLAlchemyError as e:
        session.rollback()
        return jsonify({'status': 'error', 'message': f'Database error: {str(e)}'}), 500
    except Exception as e:
        return jsonify({'status': 'error', 'message': f'Error in update_data_product_with_anonymized_data: {str(e)}'}), 500

@app.route('/anonymization/download/<dataset_id>', methods=['GET'])
def download_anonymized_data(dataset_id):
    global anonymized_dataset_table, engine

    try:
        # Fetch the dataset from the database using the unique ID
        dataset = session.query(anonymized_dataset_table).filter_by(unique_id=dataset_id).first()

        if not dataset:
            return jsonify({'status': 'error', 'message': 'Dataset not found for the given ID'}), 404

        # Get the data from the dataset (assuming it's stored as JSONB in the database)
        anonymized_data = dataset.data  # JSON object

        # Get the requested file format from query parameters (default is CSV)
        file_format = request.args.get('format', 'csv').lower()

        if file_format not in ['csv', 'json', 'xlsx']:
            return jsonify({'status': 'error', 'message': 'Invalid format. Supported formats are csv, json, xlsx.'}), 400

        # Convert the data into a DataFrame
        df = pd.DataFrame.from_records(anonymized_data)

        # Generate a file response based on the format
        output = BytesIO()
        if file_format == 'csv':
            df.to_csv(output, index=False)
            output.seek(0)
            return send_file(
                output,
                mimetype='text/csv',
                as_attachment=True,
                download_name="anonymized_dataset.csv"
            )
        elif file_format == 'json':
            output.write(df.to_json(orient='records').encode('utf-8'))
            output.seek(0)
            return send_file(
                output,
                mimetype='application/json',
                as_attachment=True,
                download_name="anonymized_dataset.json"
            )
        elif file_format == 'xlsx':
            with pd.ExcelWriter(output, engine='xlsxwriter') as writer:
                df.to_excel(writer, index=False, sheet_name='Anonymized Data')
            output.seek(0)
            return send_file(
                output,
                mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                as_attachment=True,
                download_name="anonymized_dataset.xlsx"
            )
    except Exception as e:
        logging.error(f"Error in download_anonymized_data: {str(e)}")
        return jsonify({'status': 'error', 'message': f'Error in download_anonymized_data: {str(e)}'}), 500     
    
@app.route('/anonymization/download_anonymized_file', methods=['GET'])
def download_anonymized_file():
    global anonymized_data  # Ensure you access the global variable for the anonymized dataset
    
    try:
        # Get the desired file format from query parameters
        file_format = request.args.get('format', 'csv').lower()
        
        if file_format not in ['csv', 'xlsx', 'json']:
            return jsonify({'status': 'error', 'message': 'Invalid file format'}), 400
        
        # Pass the correct DataFrame (anonymized_data) to generate the file
        return generate_anonymized_file(file_format, anonymized_data)
    except Exception as e:
        return jsonify({'status': 'error', 'message': f'Error: {str(e)}'}), 500
    
def generate_anonymized_file(file_format, data):
    output_buffer = io.BytesIO()
    if file_format == 'csv':
        # Write the anonymized DataFrame to CSV format
        pd.DataFrame(data).to_csv(output_buffer, index=False)  # Create DataFrame from anonymized data
        content_type = 'text/csv'
        file_extension = 'csv'
    elif file_format == 'xlsx':
        # Write the anonymized DataFrame to Excel format
        pd.DataFrame(data).to_excel(output_buffer, index=False)  # Create DataFrame from anonymized data
        content_type = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        file_extension = 'xlsx'
    elif file_format == 'json':
        output_buffer.write(pd.DataFrame(data).to_json(orient='records').encode())  # Create DataFrame from anonymized data
        content_type = 'application/json'
        file_extension = 'json'
    else:
        return jsonify({'status': 'error', 'message': 'Unsupported file format'}), 400

    # Seek to the beginning of the buffer
    output_buffer.seek(0)

    # Create response
    response = make_response(output_buffer.getvalue())

    # Set headers for file download
    response.headers["Content-Disposition"] = f"attachment; filename=anonymized_dataset.{file_extension}"
    response.headers["Content-type"] = content_type

    return response


"""
    # Send the anonymized CSV file as a response
    return send_file(
    io.BytesIO(anonymized_csv_data.encode()),
    mimetype='text/csv',
    #attachment_filename='anonymized_data.csv',
    download_name='anonymized_data.csv'
    )
"""


"""  

def anonymize_column(column_data, anonymization_technique):
    if anonymization_technique == 'generalization':
        return column_data.apply(lambda x: 'aris' if str(x).lower().strip() != '' else x)
    elif anonymization_technique == 'masking':
        return column_data.apply(lambda x: 'paok' if str(x).lower().strip() != '' else x)
    else:
        return column_data
"""

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)




    # Run the app on your IP address (replace 'your_ip_address' with your actual IP)
    # app.run(host='0.0.0.0', port=5000, debug=True)